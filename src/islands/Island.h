#ifndef ISLAND_H
#define ISLAND_H

#include "Serializable.h"
#include "AlgoBase.h"

class World;
class Zone;
class AgentMgr;


class Island : public Serializable
{
public:
	Island(World *w);
	virtual ~Island();

	void Init();
	void Update();
	void ChangeAlgo(AlgoID algoid);

	// Transfers all Agents from other to this, and clears the other island
	void Gobble(Island *other);

	bool CanLevelUp() const;

	// Move to next zone. WARNING: must NOT be called by a worker thread!
	void LevelUp();

	inline score getScore() const { return _bestScore; }

	virtual void serialize(ProgramState& state);
	virtual bool unserialize(const std::string& cmd, const std::string& params);

	inline const AgentArray& getAgents() const { return _amgr->agents; }
	inline const Agent *getBestAgent() const { return _amgr->getBestAgent(); }


	Zone *_zone;

protected:

	void Destroy();

	AlgoBase *_algo;
	AgentMgr *_amgr;
	World * const _world;

	uint _counterAlgo; // how many iterations was the current algo used?
	uint _counterZone; // how many terations did this island spend in the curent zone?

	score _bestScore;

	AlgoID _init_algoId; // set in unserialize(), used in Init()
	bool _firstInit;

};


#endif
