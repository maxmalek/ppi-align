#ifndef WORLD_H
#define WORLD_H

class DataHolder;
class AlgoBase;
class Agent;

#include "Zone.h"
#include "Serializable.h"
#include <vector>
#include "Threading.h"
#include "MersenneTwister.h"
#include "AlgoBase.h"

class World : public IProblemSolver
{
public:
	World(const DataHolder& dh, ThreadPool *thpool);
	virtual ~World();

	virtual void Init(); // call this AFTER loading user settings
	virtual void Update();
	virtual bool IsEmpty() const;

	Zone *NewZone();

	void RetainAlgo(AlgoBase *algo);
	AlgoBase *GetAlgo(AlgoID algoid);

	void RetainIsland(Island *isl);
	Island *GetIsland();

	const DataHolder& getDataHolder() const { return _datah; }
	ThreadPool *getThreadPool() const { return _thpool; }
	inline MTRand& getRNG() { return *_rng; }
	inline const std::vector<Zone*>& getZones() const { return _zones; }
	virtual const Agent *getBestAgent() const;


	virtual void serialize(ProgramState& state);
	virtual bool unserialize(const std::string& cmd, const std::string& params);

	void InitIsland_deferred(Island *isl);

protected:
	std::vector<Zone*> _zones;
	std::vector<AlgoBase*> _algoStore[ALGO_MAX]; // stores unused algo instances
	std::vector<Island*> _deadIslands;
	const DataHolder& _datah;

	// Only ONE of the pool pointers below may be set at any time. (Or none, if MT is turned off)
	// These pointers may not be changed anymore after being set in Init()!

	// The world thread pool. Processes whole islands in parallel,
	// but each island in itself is single-threaded. -[Coarse threading]-
	ThreadPool * _thpool; 

	// The global thread pool. If this is set, each island itself updates multi-threaded,
	// but the islands are processed sequentially -[Fine threading]-
	ThreadPool * _finePool;

	Lockable _algoLock;
	uint _levelupCounter;
	MTRand *_rng;

};

#endif
