#include "Zone.h"
#include "Island.h"
#include "World.h"
#include "Threading.h"
#include "DataHolder.h"
#include "AlgoBase.h"
#include <algorithm>
#include "ProgramState.h"

static void _UpdateIsland_MT(MTRand& /*unused*/, void *p, void *, void*)
{
	((Island*)p)->Update();
}


Zone::Zone(World *w, score scoreBegin)
 : prevZone(NULL)
 , nextZone(NULL)
 , _world(w)
 , _scoreBegin(scoreBegin)
 , _scoreEnd(scoreBegin - w->getDataHolder().GetUserSettings().islands_zoneGranularity)
{
}

Zone::~Zone()
{
	for(std::list<Island*>::iterator it = _islands.begin(); it != _islands.end(); ++it)
		delete *it;
}

void Zone::Init()
{
	for(std::list<Island*>::iterator it = _islands.begin(); it != _islands.end(); ++it)
		_world->InitIsland_deferred(*it);
}

void Zone::Update()
{
	if(_islands.empty())
		return;

	_LimitSize();

	if (ThreadPool *thpool = _world->getThreadPool())
	{
		for(std::list<Island*>::iterator it = _islands.begin(); it != _islands.end(); ++it)
			thpool->addJob(_UpdateIsland_MT, *it);
	}
	else
	{
		for(std::list<Island*>::iterator it = _islands.begin(); it != _islands.end(); ++it)
			(*it)->Update();
	}
}

void Zone::_LimitSize()
{
	uint maxislands = _world->getDataHolder().GetUserSettings().islands_maxIslandsPerZone;
	if(!maxislands || _islands.size() <= maxislands)
		return;

	uint w = 0;
	Island **all = (Island**)alloca(_islands.size() * sizeof(Island*));
	for(std::list<Island*>::iterator it = _islands.begin(); it != _islands.end(); ++it)
		all[w++] = *it;

	MT_SHUFFLE(&all[0], &all[w], _world->getRNG());
	w = 0;
	while(_islands.size() > maxislands)
	{
		Island *a = all[w];
		Island *b = all[w+1];
		w += 2;
		a->Gobble(b);
		a->ChangeAlgo(ALGO_EVO);
	}
}

void Zone::DoLevelup()
{
	Island **levelupCandidates = (Island**)alloca(_islands.size() * sizeof(Island*));
	uint found = 0;
	for(std::list<Island*>::iterator it = _islands.begin(); it != _islands.end(); ++it)
	{
		Island *isl = *it;
		if(isl->CanLevelUp() && isl->getScore() < _scoreEnd)
		{
			levelupCandidates[found++] = isl;
		}
	}

	if(found >= 2)
	{
		MT_SHUFFLE(&levelupCandidates[0], &levelupCandidates[found], _world->getRNG());
		for(uint i = 0; i+1 < found; i += 2)
		{
			Island *a = levelupCandidates[i];
			Island *b = levelupCandidates[i+1];

			// WARNING: This may modify world's zone list!
			a->LevelUp();

			a->Gobble(b);
			a->ChangeAlgo(ALGO_EVO);
		}
	}
}

void Zone::Add(Island *isl)
{
	_islands.push_back(isl);
	isl->_zone = this;
}

void Zone::Remove(Island *isl)
{
	_islands.remove(isl);
	isl->_zone = NULL;
}

const Agent *Zone::getBestAgent() const
{
	const Agent *best = NULL;
	score bestscore = -1;
	for(std::list<Island*>::const_iterator it = _islands.begin(); it != _islands.end(); ++it)
	{
		const Island *isl = *it;
		const Agent *a = isl->getBestAgent();
		score sc = a->getScore();
		if(!best || sc < bestscore)
		{
			best = a;
			bestscore = sc;
		}
	}
	return best;
}



void Zone::serialize(ProgramState& state)
{
	state.AddCmd("ZONE", "");

	for(std::list<Island*>::const_iterator it = _islands.begin(); it != _islands.end(); ++it)
		(*it)->serialize(state);
}

bool Zone::unserialize(const std::string& cmd, const std::string& params)
{
	if(cmd == "ISLAND")
	{
		Add(_world->GetIsland());
		return true;
	}

	return _islands.back()->unserialize(cmd, params);
}
