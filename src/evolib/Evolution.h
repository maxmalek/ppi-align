#ifndef EVOLUTION_H
#define EVOLUTION_H

#include "AlgoBase.h"


class Evo : public AlgoBase
{
public:

	Evo(const DataHolder& dh, ThreadPool *thpool);
	virtual ~Evo();

	virtual void Update();
	virtual void Init();

	void CreateInitialPopulation(uint n);
	void Select();
	void DealDamage();
	void CreateOffspring(uint n);
	void MergeOffspring();

	// Calculates the damage for one agent stored at a position
	// in the vector. Assumes the vector is sorted, so that low
	// positions will receive low damage. (Used after Sort())
	float CalcDamageForPos(uint pos);

protected:

	virtual void _OnUnlink();

	AgentArray _nextGeneration;

	uint _dropDupCounter;

	DEF_THREADFUNC(_Init_Random_MT);
	DEF_THREADFUNC(_Mutate_MT);
	DEF_THREADFUNC(_Init_Greedy_MT);


	// Generic randomized function that produces 1 new agent
	void _InitOffspringAgent(Agent *a, MTRand& rng);

	// Randomly shuffles pairs
	void _InitMappingRandom(Agent *a, MTRand& rng);

	// Tries to produce a somewhat good mapping by assigning nodes
	// in a way that the initial pairs have the best possible pair scores
	void _InitMappingGreedy(Agent *a, MTRand& rng);

	// avg mult: used to scale the average up or down.
	// < 1: be stricter, more likely to swap
	// > 1: be more tolerant, less likely to swap
	// Overwrites agent in out.
	void _DoPMXMutation(Agent *out, MTRand& rng, score avgMult = 1, uint forcedDivider = 128);

	// Crosses a and b, writing result into out (overwriting passed agent)
	void _DoPMXCrossover2(Agent *out, MTRand& rng, const Agent *a, const Agent *b);

	// Does a crossover between multiple parents
	void _DoOctopusCrossover(Agent *out, MTRand& rng, const Agent * const *parentArr, uint parentCount);

	// Force swapping of at least one bad pair so that the resulting score will be better.
	// Returns false if it fails to find a better pair and gives up.
	// limit = number of worst pairs to find. -1 = find all.
	// use   = how many of the worst pairs to actually use (the higher, the longer this method takes.
	// use can be >= limit or -1, in that case, limit is used.
	bool _DoDirectedMutation(Agent *out, MTRand& rng, uint limit, uint use);
	// Helper function for the above.
	bool _DirectImprovePair(Agent *out, const PairInfo& pair);


};

#endif

