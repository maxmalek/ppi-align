#include "BeeColony.h"

//for threading
void BeeColony::_Random_Init_MT(MTRand& rng, void *a, void*, void*)
{
	Mutators::randomInit((Agent*)a, rng);
	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;
}
//for threading
void BeeColony::_Heuristic_Init_MT(MTRand& rng, void *a, void* mgr, void*)
{
	Mutators::heuristicInit((Agent*)a, rng, (AgentMgr*) mgr);
	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;
}

//for threading
void BeeColony::_Naive_Greedy_Mutate_MT(MTRand& rng, void *a, void *parent, void *colony)
{
	((Agent*)a)->copyFrom(*(Agent*)parent);
	Mutators::naiveGreedyMutation((Agent*)a, rng);
	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;

	((BeeColony*)colony)->_evaluate((Agent*)a, (Agent*)parent);
	((BeeColony*)colony)->_degradeAgent((Agent*)parent);
}

//for threading
void BeeColony::_Greedy_Mutate_MT(MTRand& rng, void *a, void *parent, void *colony)
{
	((Agent*)a)->copyFrom(*(Agent*)parent);
	
	if(rng.rand()<.01)
		Mutators::greedyMutation((Agent*)a, rng, 4);
	else
		Mutators::permuteWorst((Agent*)a, rng, 10);
	
	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;

	((BeeColony*)colony)->_evaluate((Agent*)a, (Agent*)parent);
	((BeeColony*)colony)->_degradeAgent((Agent*)parent);
}

void BeeColony::_Conservative_Mutate_MT(MTRand& rng, void *a, void *parent, void *colony)
{
	((Agent*)a)->copyFrom(*(Agent*)parent);
	Mutators::conservativeMutation((Agent*) a, rng);

	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;

	((BeeColony*)colony)->_evaluate((Agent*)a, (Agent*)parent);
	((BeeColony*)colony)->_degradeAgent((Agent*)parent);
}

void BeeColony::_Randomised_Greedy_Mutate_MT(MTRand& rng, void *a, void *parent, void *colony)
{
	((Agent*)a)->copyFrom(*(Agent*)parent);
	Mutators::randomisedGreedyMutation((Agent*)a, rng, 5, 10);

	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;

	((BeeColony*)colony)->_evaluate((Agent*)a, (Agent*)parent);
	((BeeColony*)colony)->_degradeAgent((Agent*)parent);
}

//for threading
void BeeColony::_Random_Mutate_MT(MTRand& rng, void *a, void *parent, void *colony)
{
	((Agent*)a)->copyFrom(*(Agent*)parent);
	Mutators::randomMutation((Agent*)a, rng);
	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;
	
	((BeeColony*)colony)->_evaluate((Agent*)a, (Agent*)parent);
	((BeeColony*)colony)->_degradeAgent((Agent*)parent);
}

void BeeColony::_Multi_Random_Mutate_MT(MTRand& rng, void *a, void *parent, void *colony)
{
	((Agent*)a)->copyFrom(*(Agent*)parent);
	
	for(uint i=0; i<2; ++i)
		Mutators::randomMutation((Agent*)a, rng);
	
	((Agent*)a)->makeDirty();
	((Agent*)a)->locked = true;
	
	((BeeColony*)colony)->_evaluate((Agent*)a, (Agent*)parent);
	((BeeColony*)colony)->_degradeAgent((Agent*)parent);
}

BeeColony::BeeColony(const DataHolder& dh, ThreadPool *thpool)
: AlgoBase(ALGO_BEES, dh, thpool)
, _dropDupCounter(0)
{
}

BeeColony::~BeeColony()
{
	/*for(size_t i = 0; i < _decayed.size(); ++i)
		delete _decayed[i];
	for(size_t i = 0; i < _agentsMutated.size(); ++i)
		delete _agentsMutated[i];*/

	// Must have been emptied by _OnUnlink()
	ASSERT(_agentsMutated.empty());
	ASSERT(_decayed.empty());
}

void BeeColony::Init()
{
	const uint workers = uint(_settings.maxAgents * 0.9);
	const uint scouts  = _settings.maxAgents - workers;

	initSwarm(workers, scouts);
}

void BeeColony::Update()
{
	doOptimization();
}

void BeeColony::_OnUnlink()
{
	_mergeAgentArrays();
	_saveAgents(true);
}


void BeeColony::initSwarm(uint w, uint s)
{
	ASSERT(w && s);

	//preparations
	BEE_noWorkers = w;
	BEE_noScouts = s;
	_stuck = 0;
	_bestScore = 1;
	_dropDupCounter = 0;

	printf("generating initial swarm\n\n");
	_occupied.reserve(BEE_noWorkers * 2 + BEE_noScouts);
	_mgr->agents.reserve(BEE_noWorkers * 2 + BEE_noScouts);
	_agentsMutated.reserve(BEE_noWorkers);
	_decayed.reserve(2 * (BEE_noScouts + BEE_noWorkers));

	//get intial solutions
	sendHeuristicScouts();

	_CalcScores();
	_sortAgents(_mgr->agents);
	_dropWorst();				//redundant
}

void BeeColony::doOptimization()
{
	//find random solutins and prepare for harvesting
	sendScouts();
	_CalcScores();
	_updateScoreSum();
	_mgr->Sort();
	_resetOccupiedStates();

	//harvest and order solutions
	harvestSolutions();
	_CalcScores();

	//drop duplicates after 10 iterations
	//expensive step!
	++_dropDupCounter;
	if(_dropDupCounter >= 10)
	{
		_dropDupCounter = 0;
		_mgr->SortForDuplicateRemoval();
		_mgr->DropDuplicates();
	}
	else
	{
		_mgr->Sort();
	}

	//check if scores got any better
	if(_bestScore <= _mgr->agents[0]->getScore())
	{
		//if not: algo is stuck(?)
		++_stuck;
	}else{
		//scores did improve
		_stuck = 0; 
		_bestScore = _mgr->agents[0]->getScore(); 
	}

	//drop agents with lowest health
	//afterwards: drop agents with bad scores
	_dropWeakest();
	_dropWorst();

	_sortAgents(_decayed);
	_trimDecayed();

	//if agent scores got worse:
	//return best dead agents
	if(_decayed[0] < _mgr->agents[0])
	{
		_saveAgents();
		_sortAgents(_mgr->agents);
	}
}

//uses heuristics to generate "good" initial solutions
void BeeColony::sendHeuristicScouts()
{
	if(_thpool)
		_thpool->assertIdle();

	AgentArray& agents = _mgr->agents;
	while(agents.size() < (BEE_noScouts + BEE_noWorkers))
	{
		//random generation
		Agent *a = _mgr->newAgent();
		a->setHealth(_settings.basicHealth);
		agents.push_back(a);

		RUN_JOB_AUTOMT(_Heuristic_Init_MT, a, this);
	}

	if(_thpool)
		_thpool->waitUntilEmpty();
}

void BeeColony::sendScouts()
{
	if(_thpool)
		_thpool->assertIdle();

	AgentArray& agents = _mgr->agents;
	while(agents.size() < (BEE_noScouts + BEE_noWorkers))
	{
		//random generation
		Agent *a =_mgr->newAgent();
		a->setHealth(_settings.basicHealth);
		agents.push_back(a);

		RUN_JOB_AUTOMT(_Random_Init_MT , a);
	}

	if(_thpool)
		_thpool->waitUntilEmpty();
}

void BeeColony::harvestSolutions()
{
	if(_thpool)
		_thpool->assertIdle();
	AgentArray& agents = _mgr->agents;
	ASSERT(agents.size());
	ASSERT(_agentsMutated.empty());

	uint i;					//picked agent
	uint steps = 0;			//number of successfully picked agents
	Agent *mutated = NULL;
	while(steps < BEE_noWorkers)
	{
		ASSERT(_mgr->agents.size() >= BEE_noWorkers);

		//get random agent
		i = rng.randInt(BEE_noWorkers + BEE_noScouts - 1);
		Agent *randAgent = _mgr->agents[i];

		ASSERT(randAgent);

		//pick agent depending on specific prop
		if(_pick(randAgent))
		{
			++steps;

			//assign to pointer
			//for agent recycling
			if(!mutated)
				mutated = _mgr->newAgent();

			//begin mutating
			//decide if greedy mutation should be applied
			//do not apply greedy multiple times on the same agent!
			if(!_occupied[i])
			{
				//apply greedy depending on case
				if(_stuck < 20)
				{
					//normal situation (state is not stuck)
					//"normal" greedy is applied to gain good results
					//chooses with prob of 10% to apply costly and slightly random mutator
					//uses "cheap" one otherwise
					RUN_JOB_AUTOMT(_Greedy_Mutate_MT , mutated, randAgent, this);
				}else{				
					//if stuck in local minimum
					//used a different heuristic to escape local minimum
					RUN_JOB_AUTOMT(_Conservative_Mutate_MT , mutated, randAgent, this);
				}
				//mark agent's position to indicate, that greedy has been applied on it
				_occupied[i] = 1;
			}
			else
			{
				//if greedy mutation has already been applied on this agent
				//swap nodes randomly
				RUN_JOB_AUTOMT(_Multi_Random_Mutate_MT , mutated, randAgent, this);
			}

			_agentsMutated.push_back(mutated);
			mutated = NULL;
		}
	}

	if(_thpool)
		_thpool->waitUntilEmpty();
	
	//add newly generated agents (from _agentsMutated) to vector of active agents
	_mergeAgentArrays();
}

//applied on newly generated agents
//compares scores of child and parent to recalculate health
void BeeColony::_evaluate(Agent *mutated, Agent *parent)
{
	score mutatedScore = mutated->getScore();
	score parentScore = parent->getScore();
	//evaluate mutation
	if(mutatedScore <= parentScore)
	{
		//good mutation: recovery
		mutated->setHealth(_settings.basicHealth);
	}
	else
	{
		//bad mutation: damage
		//TODO: tweak formula
		float newHealth = parent->getHealth();
		score scoreDiff = mutatedScore - parentScore;
		score scoreDiffAbs = _mgr->getWorstAgent()->getScore() - _mgr->getBestAgent()->getScore();
		scoreDiffAbs = scoreDiffAbs? scoreDiff : 1;			//debug

		newHealth -= _settings.maxHealthDrop * float(scoreDiff / scoreDiffAbs);
		newHealth = (newHealth < 1)? 0 : newHealth;			//quick kill
		mutated->setHealth(newHealth);
	}
}

//drop agents with low health
//TODO: drop even more bad ones, keep dead ones in _decayed
void BeeColony::_dropWeakest()
{
	bool done = false;
	float health = 0;
	uint limit = BEE_noScouts;
	uint dropped = 0;

	AgentArray& agents = _mgr->agents;
	//ASSERT(agents.size() <= limit);

	for(size_t i=0; i<agents.size(); ++i)
	{
		if(!(agents[i]->isAlive()))
		{
			agents[i]->setHealth(_settings.basicHealth);
			ASSERT(std::find(_decayed.begin(), _decayed.end(), agents[i]) == _decayed.end());
			_decayed.push_back(agents[i]);
			agents[i] = NULL;
			++dropped;
		}
	}

	while(!done)
	{
		uint remain = agents.size() - dropped;
		if(remain <= BEE_noWorkers)
			break;

		for(size_t i=0; i<agents.size(); ++i)
		{
			if(dropped >= limit)
			{
				done = true;
				break;
			}

			if(agents[i])
			{
				if(agents[i]->getHealth() <= health)
				{
					agents[i]->setHealth(_settings.basicHealth);
					ASSERT(std::find(_decayed.begin(), _decayed.end(), agents[i]) == _decayed.end());
					_decayed.push_back(agents[i]);
					agents[i] = NULL;
					++dropped;
				}
			}
		}
		health += 1;
	}

	//dont continue if nothing has been deleted
	if(!dropped)
		return;

	_mgr->Compact();
}

//drop worst agents until number of agents equals number of workers
//assume that agents are sorted!
void BeeColony::_dropWorst()
{
	AgentArray& agents = _mgr->agents;
	ASSERT(BEE_noWorkers);
	ASSERT(agents.size());
	for(uint i=agents.size()-1; i>=BEE_noWorkers; --i)
	{
		_mgr->deleteAgent(agents[i]);
		agents[i] = NULL;
	}
	_mgr->Compact();
}

void BeeColony::_sortAgents(AgentArray& arr)
{
	if(arr.empty())
		return;

	AgentPtrCmp cmp;
	std::sort(arr.begin(), arr.end(), cmp);
}

void BeeColony::_updateScoreSum()
{
	_scoreSum = 0;
	for(uint i=0; i<_mgr->agents.size(); ++i)
	{
		_scoreSum += _mgr->agents[i]->getScore();
	}
}

void BeeColony::_resetOccupiedStates()
{
	for(uint i=0; i<_occupied.size(); ++i)
		_occupied[i] = 0;
	_occupied.resize(_mgr->agents.size(), 0);
}

//decide if an agent should be picked
//depends on agent's score
bool BeeColony::_pick(Agent *agent)
{
	//calc probability to pick agent
	//good scores will have a low ratio!
	score agentScore = agent->getScore();

	score ratio = agentScore / _scoreSum;

	//generate random number
	score rand = rng.rand();

	//final decision
	return (rand <= ratio) && agent->isAlive();
}

//degrades health of agents depending on their age
//applied to agents which were used to spawn a mutated agent
void BeeColony::_degradeAgent(Agent *agent)
{
	float newHealth;
	uint age;
	//calculate new health and increase life afterwards

	//formula: age / (age + basicHealth)
	//starts near 0 for low ages and high basicHealth
	//converges to 1 for high ages
	//the older an agent gets, the more it will be degraded
	//high basicHealth makes agents more resistant to degradation
	age = agent->getLife();
	newHealth = agent->getHealth();
	newHealth *= 1 - (age / (age + _settings.maxHealthDrop));
	newHealth = (newHealth < 1)? 0 : newHealth;			//quick kill

	agent->setHealth(newHealth);
	agent->incrLife();

}

void BeeColony::_mergeAgentArrays()
{
	if(!_agentsMutated.size())
		return;

	AgentArray& agents = _mgr->agents;

	agents.reserve(agents.size() + _agentsMutated.size());
	for(size_t i=0; i<_agentsMutated.size(); ++i)
	{
		_agentsMutated[i]->locked = true;
		agents.push_back(_agentsMutated[i]);
	}
	_agentsMutated.clear();
}

//adds decayed agents if scores get worse
void BeeColony::_saveAgents(bool force /* = true */)
{
	if(!_decayed.size())
		return;

	AgentArray& agents = _mgr->agents;

	// Can't be empty here. If it is, something was messed up externally
	ASSERT(agents.size());
	if(force || _decayed[0]->getScore() < agents[0]->getScore())
	{
		for(size_t i=0; i<_decayed.size(); ++i)
		{
			agents.push_back(_decayed[i]);
		}
		_decayed.clear();
	}
}

//drops worst decayed agents
void BeeColony::_trimDecayed()
{
	if(_decayed.size() > BEE_noScouts)
	{
		for(size_t i = BEE_noScouts; i < _decayed.size(); ++i)
		{
			_mgr->deleteAgent(_decayed[i]);
			_decayed[i] = NULL;
		}

		_decayed.resize(BEE_noScouts);
	}
}
