#ifndef MUTATORS_H
#define MUTATORS_H

#include "MersenneTwister.h"
#include "Agent.h"
#include "AgentMgr.h"
#include "DataHolder.h"

#include <algorithm>

class Mutators
{
public:
	//swap all positions for random initialization
	static void randomInit(Agent *agent, MTRand& rng)
	{
		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& refMap = agent->getReferenceMapping();
		const NodeMapping& varMap = agent->getMapping();

		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();
		const size_t accessable = mapSize - prematched;

		ASSERT(prematched < mapSize);
		
		for(uint i=prematched; i<mapSize; ++i)
		{
			agent->swap(i, prematched + rng.randInt(accessable-1));
		}
	}

	//used for first mappings
	//swaps pairs in relation to their degrees
	//avoid insertions and deletions
	static void heuristicInit(Agent *agent, MTRand& rng, AgentMgr *mgr)
	{
		//random initialization to make sure, that agent is randomly initialized
		randomInit(agent, rng);

		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& refMap = agent->getReferenceMapping();
		const NodeMapping& varMap = agent->getMapping();

		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();
		const uint maxRefID = agent->GetDataHolder().GetMaxReferenceNodeID();
		
		ASSERT(prematched < mapSize);

		score graphletScore, bestScore;
		uint bestMatch, a, b;
		//int varDegree, refDegree;
		
		//first step:
		//eliminate insertions/ deletions
		for(uint i=prematched; i<mapSize; ++i)
		{
			//in case of insertion/ deletion
			//variable node is NULL
			if(!(varMap.node[i]) && refMap.node[i])
			{
				for(uint j=i; j<mapSize; ++j)
				{
					if(varMap.node[j] && !(refMap.node[j]))
					{
						agent->swap(i, j);
						continue;
					}
				}
				continue;
			}
			
			//reference node is NULL
			if(!(refMap.node[i]) && varMap.node[i])
			{
				for(uint j=i; j<mapSize; ++j)
				{
					if(refMap.node[j] && !(varMap.node[j]))
					{
						agent->swap(i, j);
						continue;
					}
				}
				continue;
			}

		}

		//second step:
		//match nodes with low graphlet distance
		for(uint i=prematched; i<mapSize; ++i)
		{
			if(!varMap.node[i])
				continue;
			
			bestMatch = i;
			bestScore = 1;

			for(uint j=i; j<mapSize; ++j)
			{
				if(!refMap.node[j])
					continue;

				a = varMap.nodeId[i];
				b = refMap.nodeId[j];
				graphletScore = holder.GetGraphletScore(a, b);
			
				if(graphletScore <= bestScore)
				{
					bestScore = graphletScore;
					bestMatch = j;
				}
			}
			agent->swap(i, bestMatch);
		}
	}

	//random swap of nodes in mapping
	static void randomMutation(Agent *agent, MTRand& rng)
	{
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());
		
		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& varMap = agent->getMapping();

		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();
		const size_t accessable = mapSize - prematched;
		
		uint a = prematched + rng.randInt(accessable-1);
		uint b = prematched + rng.randInt(accessable-1);
		while(a==b)
		{
			b = prematched + rng.randInt(accessable-1);
		}

		agent->swap(a, b);
	}

	//deprecated
	//naive greedy approach
	//swaps node with worst score
	//at one point, this sucker even leads to bad scores
	static void naiveGreedyMutation(Agent *agent, MTRand& rng)
	{	
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());

		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& varMap = agent->getMapping();

		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();
		const size_t accessable = mapSize - prematched;

		const NodeMapping& ref = agent->getReferenceMapping();

		uint indexWorst = prematched;
		score scoreWorst = agent->getPairScore(prematched);
		
		//find worst node-to-node mapping
		for(uint i=prematched+1; i<mapSize; ++i)
		{
			score nodeScore = agent->getPairScore(i);
			if(nodeScore >= scoreWorst)
			{
				scoreWorst = nodeScore;
				indexWorst = i;
			}
		}

		int swap = prematched + rng.randInt(accessable-1);
		agent->swap(indexWorst, swap);
	}

	//swaps worst nodes heuristically
	//fast, but not perfect
	static void permuteWorst(Agent *agent, MTRand& rng,  uint nodes = 4)
	{
		ASSERT(agent->sanityCheck());
		ASSERT(nodes > 1);
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());
		ASSERT(agent->getMapping().size() >= nodes);
		
		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& refMap = agent->getReferenceMapping();
		const NodeMapping& varMap = agent->getMapping();
		
		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();

		//init list with worst mappings
		//index at first, score at second position
		std::vector<std::pair<uint, score> > worst;
		worst.reserve(nodes+1);
		worst.push_back(std::make_pair(prematched, agent->getPairScore(prematched)));

		//first step:
		//find worst nodes (use graphlet distance)
		//algo does some kind of insertion sort
		// FIXME: slooow. better to insert right away and avoiding moving entries around all the time, and sort later.
		bool added;
		score graphletScore;
		uint a, b;

		for(uint i=prematched+1; i<mapSize; ++i)
		{
			a = varMap.nodeId[i];
			b = refMap.nodeId[i];
			graphletScore = holder.GetGraphletScore(a, b);
			added = false;
			for(uint j=0; j<worst.size(); ++j)
			{
				if(graphletScore>=worst[j].second)
				{
					added = true;
					worst.insert(worst.begin()+j, std::make_pair(i, graphletScore));
					break;
				}
			}
			//append entry which have not been added to end (cause they are bigger than the others!)
			if(!added && (worst.size() < nodes))
			{
				worst.push_back(std::make_pair(i, graphletScore));
				continue;
			}
			//remove redundant/ best entry to reduce running time
			if(worst.size() > nodes)
			{
				worst.pop_back();
			}
		}

		random_shuffle(worst.begin(), worst.end());

		//random permutation
		uint rand; 
		for(uint i=0; i<nodes; ++i)
		{
			rand = rng.randInt(nodes - 1); 
			agent->swap(worst[i].first, worst[rand].first);
		}

		//try to get get best possible combination
		//TODO: too sloppy
		uint bestMatch;
		score bestScore;
		for(uint i=0; i<nodes; ++i)
		{
			bestMatch = i;
			bestScore = 1;
			a = varMap.nodeId[worst[i].first];

			for(uint j=i; j<nodes; ++j)
			{
				b = refMap.nodeId[worst[j].first];
				graphletScore = holder.GetGraphletScore(a, b);
				
				if(graphletScore < bestScore)
				{
					bestScore = graphletScore;
					bestMatch = j;
				}
			}
			agent->swap(worst[i].first, worst[bestMatch].first);
		}
	}

	//swap nodes if this increases graphlet scores
	//ignore nodes with almost perfect pairscore
	//expensive
	static void conservativeMutation(Agent *agent, MTRand& rng)
	{
		ASSERT(agent->sanityCheck());
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());
		
		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& refMap = agent->getReferenceMapping();
		const NodeMapping& varMap = agent->getMapping();
		
		const size_t mapSize = varMap.size();
		const size_t reflen = holder.GetMaxReferenceNodeID() - 1;
		const size_t prematched = holder.getFirstNonPrematchedIndex();


		uint a, b, r;
		uint bestMatch;
		score bestScore, graphletScore;

		for(uint i=0; i<20; )
		{
			r = rng.randInt(mapSize-1);

			if(!varMap.node[r])
				continue;
			
			++i;

			//skip (almost) perfect match
			//if(agent->getPairScore(r)<.05)
			//	continue;

			bestMatch = r;
			bestScore = 1;
			a = varMap.nodeId[r];

			for(uint j=prematched; j<reflen; ++j)
			{
				ASSERT(refMap.node[j]);
				if(r == j)
					continue;
				
				b = refMap.nodeId[j];
				graphletScore = holder.GetGraphletScore(a, b); // TODO: pair score instead?
			
				if(graphletScore <= bestScore)
				{
					bestScore = graphletScore;
					bestMatch = j;
				}
			}
			agent->swap(r, bestMatch);
		}
	}

	//greedy mutation:
	//generate all permutations of worst nodes and so random nodes
	//will also take some random nodes into account
	//determine and apply best permutation afterwards
	//very expensive; gets trapped in local minimum quickly
	static void greedyMutation(Agent *agent, MTRand& rng,  uint nodes = 4)
	{
		ASSERT(agent->sanityCheck());
		ASSERT(nodes > 1);
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());
		ASSERT(agent->getMapping().size() >= nodes);
		
		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& refMap = agent->getReferenceMapping();
		const NodeMapping& varMap = agent->getMapping();
		
		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();

		//init list with worst mappings
		//index at first, score at second position
		std::vector<std::pair<uint, score> > worst;
		worst.reserve(nodes+1);
		worst.push_back(std::make_pair(prematched, agent->getPairScore(prematched)));

		//find worst nodes (use graphlet distance)
		//algo does some kind of insertion sort
		// FIXME: slooow. better to insert right away and avoiding moving entries around all the time, and sort later.
		bool added;
		score graphletScore;
		uint a, b;

		for(uint i=prematched+1; i<mapSize; ++i)
		{
			a = varMap.nodeId[i];
			b = refMap.nodeId[i];
			graphletScore = holder.GetGraphletScore(a, b);
			added = false;
			for(uint j=0; j<worst.size(); ++j)
			{
				if(graphletScore>=worst[j].second)
				{
					added = true;
					worst.insert(worst.begin()+j, std::make_pair(i, graphletScore));
					break;
				}
			}
			//append entries which have not been added to end (cause they are bigger than the others!)
			if(!added && (worst.size() < nodes))
			{
				worst.push_back(std::make_pair(i, graphletScore));
				continue;
			}
			//remove redundant/ best entry to reduce running time
			if(worst.size() > nodes)
			{
				worst.pop_back();
			}
		}

		ASSERT(nodes == worst.size());
		
		//add some random nodes:
		for(uint i=0; i<nodes/2; ++i)
		{
			worst.push_back(std::make_pair(rng.rand(mapSize-prematched)+prematched+1 , 0));
		}

		uint *curPerm = (uint*)alloca(worst.size() * sizeof(uint));
		for(uint i=0; i<worst.size(); ++i)
			curPerm[i] = worst[i].first;

		std::vector<std::vector<uint> > perms = johnson_trotter(curPerm, nodes);	//permutations;
		perms.pop_back();			//last entry equals original sequence --> redundant

		//find best permutation
		score bestScore = 9999;
		uchar *swapped = (uchar*)alloca(mapSize);
		uint *oriPerm = (uint*)alloca(nodes * sizeof(uint));
		memcpy(oriPerm, curPerm, nodes * sizeof(uint));
		const NodeMapping oriMap = agent->getMapping();
		NodeMapping bestPerm;

		//TODO 
		//cleanup, speedup, fix, revise
		for(uint i=0; i<perms.size(); ++i)
		{
			memset(swapped, 0, mapSize);

			agent->getMappingNonConst() = oriMap;
			agent->makeDirty();
			memcpy(curPerm, &perms[i][0], nodes * sizeof(uint));

			//apply permutation
			for(uint j=0; j<nodes; ++j)
			{
				a = oriPerm[j];
				b = curPerm[j];
				if(!(swapped[a] && swapped[b]))
				{
					agent->swap(a, b);
					swapped[a] = 1;
					swapped[b] = 1;
				}
			}
			

			const score curScore = agent->getScore();
			if(curScore <= bestScore)
			{
				bestScore = curScore;
				bestPerm = varMap;
			}
		}

		ASSERT(bestPerm.size())
		agent->getMappingNonConst() = bestPerm;
		agent->makeDirty();

		/*for(uint i=0; i<perms.size(); ++i)
		{
			permMap = varMap;
			a = permMap[curPerm[i]].nodeId;
			b = permMap[i].nodeId;
			agent->swap(a, b);

			if(agent->getScore() <= bestScore)
			{
				bestScore = agent->getScore();
				bestPerm = i;
			}
		}

		//apply best permutation
		curPerm = perms[bestPerm];
		for(uint i=0; i<nodes; ++i)
		{	
			a = varMap[curPerm[i]].nodeId;
			b = varMap[i].nodeId;
			agent->swap(a, b);
		}*/

		ASSERT(agent->sanityCheck());
	}

	static uint factorial(uint n)
	{
		uint res = 1;
		for(uint i=2; i<=n; ++i)
			res *= i;
		return res;
	}

	//generates all permutations of input sequence
	static std::vector<std::vector<uint> > johnson_trotter(const uint *seq, size_t seqsize)
	{
		//initialization
		uint pos, s, l, fac;
		fac = factorial(seqsize);
		std::vector<std::vector<uint> > perms, tmp;
		perms.reserve(fac);
		tmp.reserve(fac);

		std::vector<uint> first;
		first.push_back(seq[0]);
		perms.push_back(first);

		if(seqsize <= 1)
			return perms;

		//for each letter in original sequence
		for(uint i=1; i<seqsize; ++i)
		{
			//preparation
			tmp.clear();

			s = perms.size();
			l = perms[0].size();
			for(uint j=0; j<s; ++j)
			{
				for(uint k=0; k<=l; ++k)
				{
					tmp.push_back(perms[j]);
				}
			}
			perms.swap(tmp);

			pos = 0;
			for(uint j=0; j<perms.size(); ++j)
			{
				perms[j].insert((perms[j].begin()+pos), seq[i]);
				++pos;
				if(pos >= perms[j].size())
				{
					pos = 0;
				}
			}
		}
		return perms;
	}

	//maps worst pair to NULL-entry (deletion)
	static void destroyWorst(Agent *agent, MTRand& rng)
	{
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());

		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& refMap = agent->getReferenceMapping();
		const NodeMapping varMap = agent->getMapping();

		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();
		
		uint nullEntry = prematched + 1; 
		uint indexWorst = prematched;
		score scoreWorst = agent->getPairScore(prematched);
		
		//find worst node-to-node mapping
		for(uint i=prematched+1; i<mapSize; ++i)
		{
			score nodeScore = agent->getPairScore(i);
			if(nodeScore >= scoreWorst)
			{
				scoreWorst = nodeScore;
				indexWorst = i;
			}
		}

		std::vector<uint> nullEntries;
		for(uint i=prematched+1; i<mapSize; ++i)
		{
			if(!refMap.nodeId[i])
				nullEntries.push_back(i);
		}
		
		//get random nullentry
		nullEntry = nullEntries[rng.randInt(nullEntries.size()-1)];

		agent->swap(indexWorst, nullEntry);
	}

	//forced mutation
	//tries to find a better match for a node 
	static void forcedGreedyMutation(Agent *agent, MTRand& rng)
	{
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());

		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& varMap = agent->getMapping();

		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();

		destroyWorst(agent, rng);

		for(uint i=prematched+1; i<mapSize; ++i)
		{
			score curScore = holder.GetCombinedScore(i, i);
			for(uint j=i+1; j<mapSize; ++j)
			{
				if(holder.GetCombinedScore(i, j) < curScore)
				{
					agent->swap(i,j);
					return;
				}
			}
		}
		//printf("\n\nnothing changed\n\n");	//debug
	}

	static void randomisedGreedyMutation(Agent *agent, MTRand& rng, uint nodes = 4, uint randNodes = 4)
	{
		ASSERT(agent->sanityCheck());
		ASSERT(nodes > 1);
		ASSERT(agent->getMapping().size() == agent->getReferenceMapping().size());
		ASSERT(agent->getMapping().size() >= nodes);
		
		const DataHolder &holder = agent->GetDataHolder(); 
		const NodeMapping& refMap = agent->getReferenceMapping();
		const NodeMapping varMap = agent->getMapping();
		
		const size_t mapSize = varMap.size();
		const size_t prematched = holder.getFirstNonPrematchedIndex();
		const size_t accessable = mapSize - prematched;

		//init list with worst mappings
		//index at first, score at second position
		std::vector<std::pair<uint, score> > worst;
		worst.reserve(nodes+1);
		worst.push_back(std::make_pair(prematched, agent->getPairScore(prematched)));

		//first step:
		//find worst nodes (use graphlet distance)
		//algo does insertion sort
		// FIXME: slooow. better to insert right away and avoiding moving entries around all the time, and sort later.
		bool added;
		score graphletScore;
		uint a, b;

		for(uint i=prematched+1; i<mapSize; ++i)
		{
			a = varMap.nodeId[i];
			b = refMap.nodeId[i];
			graphletScore = holder.GetGraphletScore(a, b);
			added = false;
			for(uint j=0; j<worst.size(); ++j)
			{
				if(graphletScore>=worst[j].second)
				{
					//TODO: improve
					if(rng.rand() <= .5)
					{
						added = true;
						worst.insert(worst.begin()+j, std::make_pair(i, graphletScore));
						break;
					}
				}else
				{
					//TODO: improve
					if(rng.rand() <= .05)
					{
						added = true;
						worst.insert(worst.begin()+j, std::make_pair(i, graphletScore));
						break;
					}
				}
			}
			//append entry which have not been added to end (cause they are bigger than the others!)
			if(!added && (worst.size() < nodes))
			{
				worst.push_back(std::make_pair(i, graphletScore));
				continue;
			}
			//remove redundant/ best entry to reduce running time
			if(worst.size() > nodes)
			{
				worst.pop_back();
			}
		}

		//add some random nodes
		//bool inList;
		uint rand;
		for(uint i=0; i<randNodes; ++i)
		{
			rand = rng.randInt(accessable - 1);
			//check if random node already among worst
			/*inList = true;
			while(!inList)
			{
				inList = false;
				for(uint j=0; j<nodes; ++j)
				{
						if(worst[j].first == rand)
						{
							inList = true;
							break;
						}
				}
			}*/

			worst.push_back(std::make_pair(rand, 0));
		}

		//try to get get best possible combination
		//TODO imrove me
		uint bestMatch;
		score bestScore;
		for(uint i=0; i<nodes; ++i)
		{
			bestMatch = i;
			bestScore = 1;
			a = varMap.nodeId[worst[i].first];

			for(uint j=i; j<nodes; ++j)
			{
				b = refMap.nodeId[worst[j].first];
				graphletScore = holder.GetGraphletScore(a, b);
				
				if(graphletScore < bestScore)
				{
					bestScore = graphletScore;
					bestMatch = j;
				}
			}
			agent->swap(worst[i].first, worst[bestMatch].first);
		}
		
		ASSERT(agent->sanityCheck());
	}

};

#endif
