#include "common.h"
#include "BeeColony.h"
#include "Threading.h"
#include "ProgramState.h"
#include "ResultPrinter.h"
#include "CmdlineParser.h"
#include "CmdHelp.h"

#include <signal.h>
#include <fstream>

void  onSignal(int s)
{
	fprintf(stderr, "Caught signal %d\n", s);

	exit(s);
}

static void usage()
{
	puts("NABEECO v0.1 -- Builtin help:");
	CmdHelp::Print();
	puts(
		"Example:\n"
		"$ ./nabeeco --resume test.state --create --groups DM hprd --ntw DM.ntw\n"
		" --ntw hprd.ntw --grsig hprd.sigs hprd --grsig DM.sigs DM --undirected\n"
		" --pop 1000 --maxsame 500 --no-prematch\n"
		"\n"
		"See the README for additional information and input file format descriptions."
		);
}

int main(int argc, char  **argv)
{
	if(argc <= 1)
	{
		usage();
		return 0;
	}

	//signal handler
	signal(SIGFPE, onSignal);

	//parameters
	const uint BEST_AGENTS = 5;

	ThreadPool thpool;
	ProgramState state(&thpool);

	// FIXME
	state.settings.basicHealth = 50;
	state.settings.maxAgents = 500;

	CmdlineParser cmdline(argc, argv);
	if(!cmdline.Apply(state))
	{
		puts("Run the program with no parameters for a quick help.\n");
		return 2;
	}

	BeeColony bees(state.dh, &thpool);
	AgentMgr amgr(state.dh);
	bees.Link(amgr);


	if(state.Init(bees))
	{
		printf("starting bee colony optimization:\n");

		const AgentArray& agents = bees.GetAgents();
		bool stop = false;
		while(!(ProgramState::MustQuit() || bees.perfectMatch() || stop))
		{
			bees.Update();
			state.NextIteration();
			
			printf("Done round #%u\n", state.GetIterationCount());
			uint limit = std::min<uint>(BEST_AGENTS, agents.size());
			for(uint i=0; i<limit; ++i)
			{
				Agent *a = agents[i];
				printf("GED: %f, EC: %f, PS: %f, raw GED: [%u, %u], life: %u, health: %.1f\n",
					a->getGEDScore(), a->getEdgeCorrectness(), a->getPairSumScore(),
					a->getGEDRawFull(), a->getGEDRawPartial(), a->getLife(), a->getHealth());
			}

			stop = state.IsDone();
		}

		state.Shutdown();
	}
	else
	{
		printf("Something isn't right, exiting. Re-check your settings.\n");
	}

	bees.Unlink();

	return 0;
}
