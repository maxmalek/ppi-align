#include "ProteinHolder.h"
#include "UserSettings.h"
#include "tools.h"

bool ProteinHolder::_ReadBLASTMatrixBin(const std::string& fn)
{
	return _blast.readFromFile(fn.c_str());
}

bool ProteinHolder::_WriteBLASTMatrixBin(const std::string& fn)
{
	if(_settings.noWorkfiles)
		return false;
	return _blast.writeToFile(fn.c_str(), true);
}

//--------------------------------------

// This function is supposed to generate a unique name for any given network.
// Two separate checksums are used:
// - One for protein names
// - One for the edge configuration of each node
static std::string _MakeConnectivityMatrixFilename(const PNodeMap& m)
{
	uint adler = adler32(0, NULL, 0);
	uint adler2 = adler;

	char buf[64];

	for(PNodeMap::const_iterator it = m.begin(); it != m.end(); ++it)
	{
		ProteinNode *node = it->second;
		adler = adler32(adler, node->name().c_str(), node->name().length() + 1); // include '\0'
		sprintf(buf, "[%u/%u/%u]",
			(uint)node->nb.size(), (uint)node->outgoing.size(), (uint)node->incoming.size());
		adler2 = adler32(adler2, buf, strlen(buf));
	}

	sprintf(buf, "conn-matrix-%u-%u.bin", adler, adler2);
	return buf;
}

bool ProteinHolder::_ReadConnectivityMatrixBin(uint g)
{
	std::string fn = _MakeConnectivityMatrixFilename(_grpN[g]);
	return _conn[g].readFromFile(fn.c_str());
}

bool ProteinHolder::_WriteConnectivityMatrixBin(uint g)
{
	if(_settings.noWorkfiles)
		return false;
	std::string fn = _MakeConnectivityMatrixFilename(_grpN[g]);
	return _conn[g].writeToFile(fn.c_str(), true);
}
