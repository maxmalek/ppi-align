#include "ResultPrinter.h"
#include "ProgramState.h"
#include "Agent.h"
#include "GEDScore.h"
#include <algorithm>
#include <utility>

template <typename T> struct SortableHelper
{
	SortableHelper(score s, const T& e)
		: val(s)
		, item(e)
	{
	}

	inline bool operator< (const SortableHelper& o) const
	{
		return val < o.val;
	}

	score val;
	T item;
};


typedef std::vector<SortableHelper<uint> > MappingSorter;


void ResultPrinter::AsList(const ProgramState& state, std::ostream& o)
{
	const Agent *dude = state.GetWorkObject()->getBestAgent();

	const NodeMapping& my = dude->getMapping();
	const NodeMapping& ref = dude->getReferenceMapping();

	uint aligned = 0, left1 = 0, left2 = 0;

	for(size_t i = 0; i < my.size(); ++i)
	{
		const ProteinNode *a = my.node[i];
		const ProteinNode *b = ref.node[i];

		if(a && !b)
			++left2;
		if(b && !a)
			++left1;
		if(a && b)
			++aligned;
	}

	// TODO: algorithm used, parameters, compilation date, ...

	o << "## Group 1:                " << state.ph.GetGroupName(0) << std::endl;
	o << "## Group 2:                " << state.ph.GetGroupName(1) << std::endl;
	o << "## Network 1 size:         " << state.ph.GetGroup(0).size()<< std::endl;
	o << "## Network 1 edges:        " << state.dh.GetEdgeAmount(0)  << std::endl;
	o << "## Network 2 size:         " << state.ph.GetGroup(1).size()<< std::endl;
	o << "## Network 2 edges:        " << state.dh.GetEdgeAmount(1)  << std::endl;
	o << "## Aligned proteins:       " << aligned                    << std::endl;
	o << "## Unaligned in 1:         " << left1                      << std::endl;
	o << "## Unaligned in 2:         " << left2                      << std::endl;
	o << "##" << std::endl;
	o << "## Edge correctness:       " << (dude->getEdgeCorrectness() * 100) << "%" << std::endl;
	o << "## Iterations:             " << state.GetIterationCount()  << std::endl;
	o << "## GED score:              " << dude->getGEDScore()        << std::endl;
	o << "## Avg. pair score:        " << dude->getPairSumScore()    << std::endl;
	o << "##" << std::endl;
	o << "## Unaligned interactions: " << dude->getGEDRawFull()      << std::endl;
	o << "## Partially aligned:      " << dude->getGEDRawPartial()   << std::endl;
	o << "## Fully aligned:          " << dude->getGEDRawSubstituted()<< std::endl;
	o << "##" << std::endl;
	o << "## (Lower pair score value is better)" << std::endl;
	o << "## FA = fully aligned" << std::endl;
	o << "## PA = partially aligned (undirected->directed or vice versa)" << std::endl;
	o << "## Add = added edge" << std::endl;
	o << "## Rm = removed edge" << std::endl;
	o << "## Flip = flipped edge direction" << std::endl;
	o << "## P1\tP2\tPairscore\tFA\tPA\tAdd\tRm\tFlip\tExtra" << std::endl;
	o << std::endl;

	MappingSorter v;

	for(size_t i = 0; i < my.size(); ++i)
	{
		const ProteinNode *a = my.node[i];
		const ProteinNode *b = ref.node[i];

		if(!(a || b))
			continue;

		v.push_back(SortableHelper<uint>(dude->getPairScore(i), i));
	}

	std::sort(v.begin(), v.end());

	for(size_t i = 0; i < v.size(); ++i)
	{
		const uint pairIdx = v[i].item;
		const ProteinNode *a = my.node[pairIdx];
		const ProteinNode *b = ref.node[pairIdx];

		o <<         (a ? a->name() : "-");
		o << '\t' << (b ? b->name() : "-");
		o << '\t' << v[i].val;

		GEDInfo pairged;
		dude->getPairGED(pairIdx, pairged);
		o << '\t' << GEACC_SUBST(pairged);
		o << '\t' << (GEACC_D_TO_U(pairged) + GEACC_U_TO_D(pairged));
		o << '\t' << GEACC_ADDED(pairged);
		o << '\t' << GEACC_REMOVED(pairged);
		o << '\t' << GEACC_FLIP(pairged);

		o << '\t';
		if(a && a->prematched)
			o << "prematched";
		else
			o << '-';

		o << std::endl;
	}
}

