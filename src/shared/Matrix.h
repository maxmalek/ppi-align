#ifndef MATRIX_H
#define MATRIX_H

#include <vector>
#include "common.h"
#include "lzf.h"
#include "tools.h"

// Matrix class
// Access mode: Natural screen coordinates, unlike the "normal", still weird matrix addressing mode as used in math. Means:
// (0, 0) is in upper left corner.
// Matrix(4,2) is a matrix with x=4 columns and y=2 rows
//  +----> [X]
//  |1234
//  |5678
//  v
// [Y]
// In this example, M(0,3) == 3; and M(1,2) == 6.

template <typename T> class Matrix
{
public:
	Matrix(): _nx(0), _ny(0) {}
	Matrix(uint nx, uint ny) { resize(nx, ny); } // ctor, leaves values uninitialized
	Matrix(uint nx, uint ny, T d) { resize(nx, ny); std::fill(_data.begin(), _data.end(), d); } // ctor, initializes all fields with value
	Matrix(const Matrix& m) : _nx(m._nx), _ny(m._ny), _data(m._data) {} // copy ctor
	~Matrix() {}
	void resize(uint nx, uint ny) { _nx = nx; _ny = ny; _data.resize(nx * ny); } // Note: this causes a frame shift in values if nx is changed.
	void fillArray(T *ptr, uint size = -1); // fill with memory, up to size array positions. size will be truncated if its larger then the matrix can hold
	void fill(const T& v) { std::fill(_data.begin(), _data.end(), v); } // fill with value
	inline T& operator() (uint x, uint y) { ASSERT(x < _nx); ASSERT(y < _ny); return _data[y * _nx + x]; } // access matrix at (x, y)
	inline const T& operator() (uint x, uint y) const { ASSERT(x < _nx); ASSERT(y < _ny); return _data[y * _nx + x]; }
	inline Matrix& operator= (const Matrix& m) { if(this != &m) { _nx = m._nx; _ny = m._ny; _data = m._data; } return *this; }
	inline bool operator== (const Matrix& m) const { return _nx == m._nx && _ny == m._ny && std::equal(_data.begin(), _data.end(), m._data.begin()); }
	inline bool operator!= (const Matrix& m) const { return !(*this == m); }
	template <typename PRED> inline bool equals(const Matrix& m, PRED pred) const // equal if pred returns 0. see std::equal. [hint: use this if T==float]
		{ return _nx == m._nx && _ny == m._ny && std::equal(_data.begin(), _data.end(), m._data.begin(), pred); }
	inline void clear() { _nx = _ny = 0; _data.clear(); }

	inline uint getW() const { return _nx; } // width;  X coord
	inline uint getH() const { return _ny; } // height; Y coord

	// WARNING: use this only for POD types!
	bool writeToFile(const char *fn, bool pack = false);
	bool readFromFile(const char *fn);

	void forceDeallocate() { clear(); std::vector<T>().swap(_data); } // "swap trick"

protected:
	std::vector<T> _data;
	uint _nx, _ny;
};


template <typename T> void Matrix<T>::fillArray(T *ptr, uint size)
{
	if(size > _data.size())
		size = _data.size();
	for(uint i = 0; i < size; ++i)
		_data[i] = ptr[i];
}


// BEWARE: code is NOT endian safe!
// (But magic will detect this and refuse to load a file with wrong endianity)

template <typename T> bool Matrix<T>::writeToFile(const char *fn, bool pack /* = false */)
{
	FILE *fh = fopen(fn, "wb");
	if(!fh)
		return false;
	void *ptr = &_data[0];
	uint magic = 0x2e4d5458; // ".MTX"
	uint nullmagic = 0x00000000;
	uint flags = 0;
	uint bytesPerItem = sizeof(T);
	uint w = getW();
	uint h = getH();
	uint datasize = bytesPerItem * w * h;
	uint checksum = adler32(0, NULL, 0);
	bool good = true;
	checksum = adler32(checksum, (const char*)ptr, datasize);
	if(pack)
	{
		void *out = malloc(datasize);
		uint newsz = lzf_compress(ptr, datasize, out, datasize);
		if(newsz && newsz < datasize)
		{
			datasize = newsz;
			flags |= 0x1;
			ptr = out;
		}
		else
		{
			free(out);
		}
	}
	fwrite(&nullmagic, sizeof(uint), 1, fh); // to be replaced later, when we're sure the file was written correctly
	fwrite(&flags, sizeof(uint), 1, fh);
	fwrite(&bytesPerItem, sizeof(uint), 1, fh);
	fwrite(&datasize, sizeof(uint), 1, fh);
	fwrite(&w, sizeof(uint), 1, fh);
	fwrite(&h, sizeof(uint), 1, fh);
	fwrite(&checksum, sizeof(uint), 1, fh);
	size_t written = fwrite(ptr, 1, datasize, fh);
	if(written == datasize)
	{
		// write proper header
		fseek(fh, SEEK_SET, 0);
		fwrite(&magic, sizeof(uint), 1, fh);
	}
	else
	{
		printf("ERROR: Matrix::writeToFile(%s) - failed to write all data\n", fn);
		good = false;
	}

	if(ptr != &_data[0])
		free(ptr);
	
	fclose(fh);

	if(!good)
		remove(fn);

	return good;
}

template <typename T> bool Matrix<T>::readFromFile(const char *fn)
{
	FILE *fh = fopen(fn, "rb");
	if(!fh)
		return false;
	uint magic;
	uint flags;
	uint bytesPerItem;
	uint datasize;
	uint w;
	uint h;
	uint checksum;

	if (!fread(&magic, sizeof(uint), 1, fh)
	 || !fread(&flags, sizeof(uint), 1, fh)
	 || !fread(&bytesPerItem, sizeof(uint), 1, fh)
	 || !fread(&datasize, sizeof(uint), 1, fh)
	 || !fread(&w, sizeof(uint), 1, fh)
	 || !fread(&h, sizeof(uint), 1, fh)
	 || !fread(&checksum, sizeof(uint), 1, fh)
	 )
	{
		fclose(fh);
		return false;
	}

	if(magic != 0x2e4d5458 || bytesPerItem != sizeof(T) || !datasize)
	{
		fclose(fh);
		return false;
	}

	resize(w, h);

	uint fullsize = bytesPerItem * w * h;
	bool good = true;
	void *ptr = &_data[0];
	if(flags & 1) // packed?
	{
		ptr = malloc(datasize);
	}

	uint readbytes = fread(ptr, 1, datasize, fh);

	if(readbytes == datasize)
	{
		if(flags & 1)
		{
			uint newsz = lzf_decompress(ptr, datasize, &_data[0], fullsize);
			if(newsz != fullsize)
			{
				printf("ERROR: Matrix::readFromFile('%s') - failed to unpack\n", fn);
				good = false;
			}
			free(ptr);
		}
	}
	else
	{
		printf("ERROR: Matrix::readFromFile('%s') - failed to read enough data\n", fn);
		good = false;
	}

	fclose(fh);

	uint test = adler32(0, NULL, 0);
	test = adler32(test, (const char*)&_data[0], fullsize);
	if(test != checksum)
	{
		printf("ERROR: Matrix::readFromFile('%s') - checksum mismatch\n", fn);
		good = false;
	}

	if(!good)
	{
		_data.clear();
		_nx = _ny = 0;
	}

	return good;
}



////////////////////////////////////////////////////////

// FastMatrix - like a normal matrix, but uses bit-shifting to access elements.
// This is roughly twice as fast (as compared to multiplication used by the normal Matrix class above)
// for accessing individual elements, assuming the access operators are inlined (they are!)
// The downside is that it may use up to 2x the memory as would actually be required,
// because width needs to be rounded up to powers of 2.

// For even more efficient use, single rows can be retrieved individually and then operated on
// as arrays, if the y position stays constant (2 nested for-loops, for example)

// -- Row-wise matrix --
template <typename T> class FastMatrix
{
private:
	T *_mem;
	size_t _shift;
	size_t _w;
	size_t _h;

public:
	FastMatrix() : _mem(NULL), _shift(0), _w(0), _h(0) {}
	FastMatrix(const Matrix<T>& m) { load(m); }
	~FastMatrix() { delete [] _mem; }

	inline size_t width() const { return _w; }
	inline size_t height() const { return _h; }

	void clear()
	{
		_w = _h = _shift = 0;
		if(_mem)
		{
			delete [] _mem;
			_mem = NULL;
		}
	}

	void resize(size_t w, size_t h)
	{
		if(!(w && h))
		{
			clear();
			return;
		}
		_w = w;
		_h = h;
		size_t newsize = 1;
		size_t sh = 0;

		// find out how often we have to shift to reach the desired capacity
		// this will set the size to the nearest power of 2 required (if dim is 50 the final size will be 64, for example)
		while(newsize < w)
		{
			newsize <<= 1;
			++sh;
		}

		_shift = sh;

		if(_mem)
			delete [] _mem;

		_mem = new T[newsize * _h];

#ifndef NDEBUG
		memset(_mem, 0xFF, newsize * _h * sizeof(T)); // helps tracking down possible memory errors
#endif
	}

	void load(const Matrix<T>& m)
	{
		size_t w = m.getW();
		size_t h = m.getH();

		resize(w, h);

		for(size_t y = 0; y < h; ++y)
			for(size_t x = 0; x < w; ++x)
				(*this)(x, y) = m(x, y);
	}

	inline T& operator()(size_t x, size_t y)
	{
		ASSERT(x < _w && y < _h);
		return _mem[(y << _shift) | x];
	}

	inline const T& operator()(size_t x, size_t y) const
	{
		ASSERT(x < _w && y < _h);
		return _mem[(y << _shift) | x];
	}

	inline T *getRow(size_t y)
	{
		ASSERT(y < _h);
		return &_mem[y << _shift];
	}

	inline const T *getRow(size_t y) const
	{
		ASSERT(y < _h);
		return &_mem[y << _shift];
	}

};


// As above, but...

// For even more efficient use, single columns can be retrieved individually and then operated on
// as arrays, if the x position stays constant (2 nested for-loops, for example)

// -- Column-wise matrix --
template <typename T> class FastMatrixColumnWise
{
private:
	T *_mem;
	size_t _shift;
	size_t _w;
	size_t _h;

public:
	FastMatrixColumnWise() : _mem(NULL), _shift(0), _w(0), _h(0) {}
	FastMatrixColumnWise(const Matrix<T>& m) { load(m); }
	~FastMatrixColumnWise() { delete [] _mem; }

	inline size_t width() const { return _w; }
	inline size_t height() const { return _h; }

	void clear()
	{
		_w = _h = _shift = 0;
		if(_mem)
		{
			delete [] _mem;
			_mem = NULL;
		}
	}

	void resize(size_t w, size_t h)
	{
		if(!(w && h))
		{
			clear();
			return;
		}
		_w = w;
		_h = h;
		size_t newsize = 1;
		size_t sh = 0;

		// find out how often we have to shift to reach the desired capacity
		// this will set the size to the nearest power of 2 required (if dim is 50 the final size will be 64, for example)
		while(newsize < h)
		{
			newsize <<= 1;
			++sh;
		}

		_shift = sh;

		if(_mem)
			delete [] _mem;

		_mem = new T[newsize * _w];

#ifndef NDEBUG
		memset(_mem, 0xFF, newsize * _w * sizeof(T)); // helps tracking down possible memory errors
#endif
	}

	void load(const Matrix<T>& m)
	{
		size_t w = m.getW();
		size_t h = m.getH();

		resize(w, h);

		for(size_t y = 0; y < h; ++y)
			for(size_t x = 0; x < w; ++x)
				(*this)(x, y) = m(x, y);
	}

	inline T& operator()(size_t x, size_t y)
	{
		ASSERT(x < _w && y < _h);
		return _mem[(x << _shift) | y];
	}

	inline const T& operator()(size_t x, size_t y) const
	{
		ASSERT(x < _w && y < _h);
		return _mem[(x << _shift) | y];
	}

	inline T *getColumn(size_t x)
	{
		ASSERT(x < _h);
		return &_mem[x << _shift];
	}

	inline const T *getColumn(size_t x) const
	{
		ASSERT(x < _h);
		return &_mem[x << _shift];
	}

};

#endif
