#ifndef CMDLINEPARSER_H
#define CMDLINEPARSER_H

#include "common.h"
#include <string>

class ProgramState;

class CmdlineParser
{
public:
	CmdlineParser(int argc, char **argv);

	bool Apply(ProgramState& state);

protected:
	bool _ApplyPass(ProgramState& state, int pass);
	int _HandleEntry(int idx, ProgramState& state, int pass);
	int argc;
	const char * const *argv;
};

#endif
