#include "Agent.h"
#include "AgentMgr.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include "MersenneTwister.h"
#include "DataHolder.h"
#include "ProgramState.h"
#include "HeapArray.h"
#include "UserSettings.h"

#define AGENT_MAGIC_COOKIE 0xa5fd91b7

#ifndef NDEBUG
#  define AGENT_CHECK_PTR(who) { ASSERT(who); ASSERT((who)->_mem_cookie == AGENT_MAGIC_COOKIE); }
#else
#  define AGENT_CHECK_PTR(who)
#endif


Agent::Agent(const DataHolder& datah, const NodeMapping& m)
 : _dataH(datah)
 , _dirtyFinalScore(true)
 , _dirtyGED(true)
 , _dirtyPairs(true)
 , _dirtyRawGED(true)
 , _life(0)
 , _mapping(m)
 , _refMapping(datah.GetRefMapping())
 , _settings(datah.GetUserSettings())
 , locked(false)
 , _mem_cookie(AGENT_MAGIC_COOKIE)
{
	_rawGED.reset();

	_health = datah.GetUserSettings().basicHealth;

	dirtyTablePairs.resize(m.size(), 1); // all dirty by default
	pairScoreTable.resize(m.size());

	_indexv.resize(_dataH.GetMaxVariableNodeID());
	GEDPopulateIndex(_mapping, &_indexv[0]);
}

void Agent::copyFrom(const Agent& other)
{
	AGENT_CHECK_PTR(&other);
	_life = 0; // <-- he's fresh!
	_mapping = other._mapping;
	_indexv = other._indexv;
	pairScoreTable = other.pairScoreTable;
	dirtyTablePairs = other.dirtyTablePairs;

	_scoreGED = other._scoreGED;
	_scorePairs = other._scorePairs;

	_dirtyFinalScore = other._dirtyFinalScore;
	_dirtyGED = other._dirtyGED;
	_dirtyPairs = other._dirtyPairs;
	_dirtyRawGED = other._dirtyRawGED;

	//_health = other._health;
	_health = other._settings.basicHealth; // FIXME
	_rawGED = other._rawGED;
}

void Agent::octopusMerge(const Agent * const * parents, unsigned char *mask, uint masksize)
{
	AGENT_CHECK_PTR(this);

	_scoreGED = -999;
	_scorePairs = -999;
	_dirtyPairs = true; // Always recalculate score after crossover
	_dirtyGED = true;
	_dirtyRawGED = true;
	_dirtyFinalScore = true;

	_health = parents[0]->_settings.basicHealth;

	for(uint i = 0; i < masksize; ++i)
	{
		if(mask[i] == 0xff)
		{
			_mapping.node[i] = NULL;
			_mapping.nodeId[i] = -1; // to catch errors! MUST be overwritten by setMappingPos() (only checked in debug mode)
			pairScoreTable[i]    = -1; //invalid value
			dirtyTablePairs[i]   = 1;
		}
		else
		{
			const Agent *p = parents[mask[i]];
			AGENT_CHECK_PTR(p);
			_mapping.nodeId[i]        = p->_mapping.nodeId[i];
			_mapping.node[i]          = p->_mapping.node[i];
			pairScoreTable[i]    = p->pairScoreTable[i];
			dirtyTablePairs[i]   = p->dirtyTablePairs[i];

			const uint id = _mapping.nodeId[i];
			if(id)
				_indexv[id-1] = i;
		}
	}
}

Agent::~Agent()
{
	AGENT_CHECK_PTR(this);
	_mem_cookie = 0xfeeffeef;
}

void Agent::_CalcFinalScore() const
{
	score s = 0;
	score mult = 0;
	score w;
	if(_settings._finalCalcPairsum)
	{
		w = _settings.weightPairsum;
		mult += w;
		s += (getPairSumScore() * w);
	}
	if(_settings._finalCalcGED)
	{
		w = _settings.weightGED;
		mult += w;
		s += (getGEDScore() * w);
	}
	if(_settings._finalCalcGraphlet)
	{
		w = _settings.weightGraphlets;
		mult += w;
		s += (getGraphletSumScore() * w);
	}
	if(_settings._finalCalcNodeDist)
	{
		w = _settings.weightNodeDist;
		mult += w;
		s += (getNodeDistSumScore() * w);
	}
	if(_settings._finalCalcSeqSim)
	{
		w = _settings.weightSeqSim;
		mult += w;
		s += (getSeqSimSumScore() * w);
	}

	_scoreFinal = s / mult;
	_dirtyFinalScore = false;
	ASSERT(_scoreFinal >= 0 && _scoreFinal <= 1);
}

score Agent::getGEDScore() const
{
	AGENT_CHECK_PTR(this);
	if(_dirtyGED)
	{
		_UpdateMappingGED();
		const score worst = _dataH.getWorstGEDScore();
		
		// pretend that self loops are seen two times, i.e. similar to the otehr edges
		const score eval = 
			(_rawGED.evaluate(_dataH.getGEDScoreArray()) + _rawGED.getContributionOfSelfLoops(_dataH.getGEDScoreArray()) ) / 2;


		ASSERT(eval <= worst);



		_scoreGED = eval / worst; // This is between 0 and 1.
		ASSERT(_scoreGED >= 0 && _scoreGED <= 1);
		_dirtyGED = false;
	}
	
	return _scoreGED;
}

score Agent::getGraphletSumScore() const
{
	const size_t sz = _mapping.size();
	score s = 0;
	for (size_t i = 0; i < sz; ++i)
	{
		const uint a = _mapping.nodeId[i];
		const uint b = _refMapping.nodeId[i];
		s += _dataH.GetGraphletScore(a, b);
	}
	s /= score(sz);
	ASSERT(s >= 0 && s <= 1);
	return s;
}

score Agent::getNodeDistSumScore() const
{
	const size_t sz = _mapping.size();
	score s = 0;
	for (size_t i = 0; i < sz; ++i)
	{
		const uint a = _mapping.nodeId[i];
		const uint b = _refMapping.nodeId[i];
		s += _dataH.GetNodeDistScore(a, b);
	}
	s /= score(sz);
	ASSERT(s >= 0 && s <= 1);
	return s;
}

score Agent::getSeqSimSumScore() const
{
	const size_t sz = _mapping.size();
	score s = 0;
	for (size_t i = 0; i < sz; ++i)
	{
		const uint a = _mapping.nodeId[i];
		const uint b = _refMapping.nodeId[i];
		s += _dataH.GetSeqSimScore(a, b);
	}
	s /= score(sz);
	ASSERT(s >= 0 && s <= 1);
	return s;
}


score Agent::getPairSumScore() const
{
	AGENT_CHECK_PTR(this);
	if(_dirtyPairs)
	{
		size_t sz = _mapping.size();
		score sum = 0;
		for (size_t i = 0; i < sz; ++i)
			sum += getPairScore(i); // TODO: optimize this; add in GED later so it's less float operations.
		_scorePairs = sum / sz;
		_dirtyPairs = false;
		ASSERT(_scorePairs >= 0 && _scorePairs <= 1);
	}
	return _scorePairs;
}

void Agent::getPairGED(uint i, GEDInfo& pair) const
{
	AGENT_CHECK_PTR(this);

	calcPairGEDIndexed(_dataH, _mapping, _refMapping, i, pair, &_indexv[0]);
}

void Agent::_UpdateMappingGED() const
{
	AGENT_CHECK_PTR(this);

	if(_dirtyRawGED)
	{
		_rawGED.reset();
		calcWholeGEDIndexed(_dataH, _mapping, _refMapping, _rawGED, &_indexv[0]);
		_dirtyRawGED = false;
	}
}

// return score between 0 (perfect match) and > 0 (more diverging)
score Agent::getPairScore(uint i) const
{
	AGENT_CHECK_PTR(this);

	if(dirtyTablePairs[i])
	{
		const uint a = _mapping.nodeId[i];
		const uint b = _refMapping.nodeId[i];

		score s = _dataH.GetCombinedScore(a, b);

		if(_settings._pairCalcGED)
		{
			uint totalDegree = 0;
			if(a)
				totalDegree += (uint)_mapping.node[i]->getDegree();
			if(b)
				totalDegree += (uint)_refMapping.node[i]->getDegree();

			// Prevent division by zero for nodes without any edges
			if(totalDegree)
			{
				GEDInfo pg;
				getPairGED(i, pg);

				const score eval = pg.evaluate(_dataH.getGEDScoreArray());

				// The combined score matrix is NOT pre-scaled,
				// so we need to add the value here and scale it (see below), so it'll be in [0...1]
				s += (eval / score(totalDegree)) * _settings.pairWeightGED;
			}
		}

		s /= _settings.getPairWeightSum();
		ASSERT(s >= 0 && s <= 1);

		pairScoreTable[i] = s;
		dirtyTablePairs[i] = 0;
		return s;
	}
	else
	{
		VERIFY_BLOCK
		{
			dirtyTablePairs[i] = 1;
			ASSERT(pairScoreTable[i] == getPairScore(i));
		}
		return pairScoreTable[i];
	}
}

score Agent::getPairGraphletScore(uint i) const
{
	return _dataH.GetGraphletScore(_mapping.nodeId[i], _refMapping.nodeId[i]);
}

// debug break if debugger attached, return false in release mode
#define BAIL(err)  {fprintf(stderr, "SANITY CHECK FAILED: \"%s\", File: %s:%u\n", err, __FILE__, __LINE__); TriggerBreakpoint(); return false;}
#define BAIL_FMT(err, ...) { char errbuf[256]; sprintf(errbuf, err, __VA_ARGS__); BAIL(errbuf); }
#define ASSUME(x) {if(!(x)) BAIL(#x)}

bool Agent::sanityCheck() const
{
	AGENT_CHECK_PTR(this);

	size_t sz = _mapping.size();
	ASSUME(sz == _refMapping.size());

	// first node has id 1 ... means node N has id N
	uint maxId = _dataH.GetMaxVariableNodeID();
	unsigned char *seen = (unsigned char *)malloc(sz); // be sure the array is large enough for all following operations
	memset(seen, 0, maxId);
	bool wasPrematched = true;
	// index 0 will be used for node 1, etc.
	for(size_t i = 0; i < sz; ++i)
	{
		const ProteinNode * const node = _mapping.node[i];
		const uint nodeId = _mapping.nodeId[i];
		if(node)
		{
			ASSUME(nodeId > 0);
			ASSUME(node->id == nodeId);

			uint k = nodeId - 1;
			if(seen[k])
			{
				BAIL_FMT("Node %u exists twice (k = %u)!\n", nodeId, k);
			}
			seen[k] = 1;

			// make sure prematched nodes appear only at beginning of mapping
			ASSUME(!(node->prematched && !wasPrematched));

			if(!node->prematched)
				wasPrematched = false;

			//ASSERT(!_refMapping[i].node || !e.node->prematched); // this would prevent pre-matching against NULL, which is rather a feature than a problem.
			ASSUME(!_refMapping.node[i] || (_refMapping.node[i]->prematched == node->prematched));
		}
		else
			ASSUME(nodeId == 0);
	}

	for(uint i = 0; i < maxId; ++i)
	{
		if(seen[i] != 1)
		{
			BAIL_FMT("Node %u does not exist!\n", i+1);
		}
	}

	// Check index vector
	memset(seen, 0, sz);

	for(size_t i = 0; i < _indexv.size(); ++i)
	{
		uint idx = _indexv[i];
		if(idx >= sz)
		{
			BAIL_FMT("Index vector out of range: Is %u but should be < %u\n", idx, (uint)sz);
		}
		if(!_mapping.node[idx])
		{
			BAIL_FMT("Index vector points to NULL-node (%u at %u)\n", idx, (uint)i);
		}
		if(_mapping.node[idx] && _mapping.nodeId[idx] != i + 1)
		{
			BAIL_FMT("Index vector has wrong entry (%u at %u, should be %u)\n", idx, (uint)i, _mapping.nodeId[idx] - 1);
		}
		if(seen[idx])
		{
			printf("Index vector duplicate (%u at index %u)!\n", idx, (uint)i);
			ASSERT(false);
		}
		seen[idx] = 1;
	}
	for(size_t i = 0; i < _indexv.size(); ++i)
	{
		if(!seen[_indexv[i]])
		{
			BAIL_FMT("Index vector missing entry (%u at index %u)\n", _indexv[i], (uint)i);
		}
	}
	free(seen);

	// Create index vector on the fly and make sure it matches the current one
	uint *indexv = (uint*)alloca(_indexv.size() * sizeof(uint));
	GEDPopulateIndex(_mapping, indexv);
	for(size_t i = 0; i < _indexv.size(); ++i)
		ASSUME(_indexv[i] == indexv[i]);

	return true;
}
#undef BAIL
#undef BAIL_FMT
#undef ASSUME

void Agent::makeDirty()
{
	ASSERT(!locked);

	_dirtyFinalScore = true;
	_dirtyGED = true;
	_dirtyPairs = true;
	_dirtyRawGED = true;

	memset(&dirtyTablePairs[0], 1, dirtyTablePairs.size());

	GEDPopulateIndex(_mapping, &_indexv[0]);
}

void Agent::_setMappingPos(uint pos, const ProteinNode *node)
{
	AGENT_CHECK_PTR(this);
	ASSERT(!locked);
	ASSERT(_mapping.node[pos] == NULL);
	ASSERT(_mapping.nodeId[pos] == uint(-1)); // set by ctor for this special case
	_mapping.node[pos] = node;
	const uint id = node ? node->id : 0;
	_mapping.nodeId[pos] = id;

	// Fix index vector
	if(id)
		_indexv[id-1] = pos;

	// set by ctor
	//dirtyTablePairs[pos] = 1;
	//dirtyTableGED[pos] = 1;
}

void Agent::serialize(ProgramState& state)
{
	AGENT_CHECK_PTR(this);

	std::ostringstream strm;
	for(size_t i = 0; i < _mapping.size(); ++i)
	{
		strm << _mapping.nodeId[i] << ' ';
	}
	state.AddCmd("AGENT", strm.str());

	char buf[128];

	sprintf(buf, "%u", _life);
	state.AddCmd("AGENT_LIFE", &buf[0]);

	sprintf(buf, "%g", _health);
	state.AddCmd("AGENT_HEALTH", &buf[0]);
}

bool Agent::unserialize(const std::string& cmd, const std::string& params)
{
	AGENT_CHECK_PTR(this);

	if(cmd == "AGENT_LIFE")
	{
		_life = atoi(params.c_str());
		return true;
	}

	if(cmd == "AGENT_HEALTH")
	{
		_health = (float)strtod(params.c_str(), NULL);
		return true;
	}

	return false;
}

uint Agent::getDistanceTo(const Agent *other) const
{
	AGENT_CHECK_PTR(this);
	AGENT_CHECK_PTR(other);

	uint d = 0;
	size_t sz = _mapping.size();
	const NodeMapping& om = other->_mapping;
	for(size_t i = 0; i < sz; ++i)
		if(_mapping.nodeId[i] != om.nodeId[i])
			++d;
	return d;
}

// aka. bigger score first
struct _WorsePairFirst
{
	inline bool operator() (const PairInfo& a, const PairInfo& b) const
	{
		return a.scor > b.scor;
	}
};

void Agent::getWorstPairs(PairInfo *p, uint n, PairwiseScoringMethod mth) const
{
	AGENT_CHECK_PTR(this);
	ASSERT(n);


	// If we just need to get all pairs, there is no special need for optimization
	if(n == _mapping.size())
	{
		for(uint i = _dataH.getFirstNonPrematchedIndex(); i < n; ++i)
		{
			p[i] = PairInfo((this->*mth)(i), i);
		}
	}
	else
	{
		// ... Otherwise, use a heap instead of getting all values and then sorting
		for(uint i = 0; i < n; ++i)
		{
			p[i].index = -1;
			p[i].scor = -1;
		}

		HeapArray<PairInfo, _WorsePairFirst> heap(p, n);

		size_t sz = _mapping.size();
		for(size_t i = _dataH.getFirstNonPrematchedIndex(); i < sz; ++i)
		{
			heap.Add(PairInfo((this->*mth)(i), i));
		}
	}
}

score Agent::getEdgeCorrectness() const
{
	_UpdateMappingGED();

	const uint minEdges = std::min(_dataH.GetVariableEdgeAmount(), _dataH.GetReferenceEdgeAmount());

	// FIXME: this only counts 100% correct edges.
	// For undirected graphs this is simply the amount of substituted edges,
	// but for directed graphs, this is currently only the amount of edges which were aligned to have
	// the same direction in both graphs.
	// -> Flipped or direction-changed edges are NOT counted here!
	uint sameEdges = GEACC_SUBST(_rawGED);

	ASSERT(!(sameEdges & 1)); // we've seen each edge in question twice
	sameEdges >>= 1;

	ASSERT(sameEdges <= minEdges);

	return score(sameEdges) / score(minEdges);
}

