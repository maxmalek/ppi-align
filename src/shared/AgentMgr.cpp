#include "AgentMgr.h"
#include "DataHolder.h"
#include <sstream>
#include <algorithm>

AgentMgr::AgentMgr(const DataHolder& dh)
 : _dataH(dh)
{
}

AgentMgr::~AgentMgr()
{
	for(size_t i = 0; i < agents.size(); ++i)
		delete agents[i];
	for(size_t i = 0; i < _deadAgents.size(); ++i)
		delete _deadAgents[i];
}

const Agent *AgentMgr::GetRandomAgent(MTRand& rng) const
{
	ASSERT(agents.size());
	const Agent *a = agents[rng.randInt(agents.size() - 1)];
	ASSERT(a);
	return a;
}

const Agent *AgentMgr::GetRandomAgent(MTRand& rng, uint limit) const
{
	ASSERT(agents.size());
	const Agent *a = agents[rng.randInt(std::min<uint>(limit, agents.size() - 1))];
	ASSERT(a);
	return a;
}

void AgentMgr::serialize(ProgramState& state)
{
	for(size_t i = 0; i < agents.size(); ++i)
	{
		agents[i]->serialize(state);
	}
}

bool AgentMgr::unserialize(const std::string& cmd, const std::string& params)
{
	if(cmd == "AGENT")
	{
		NodeMapping m;
		const NodeMapping& defaultM = _dataH.GetDefaultMapping();
		m.reserve(defaultM.size());
		std::istringstream in(params);
		uint nodeId = 0;
		while(in >> nodeId)
		{
			m.push_back(nodeId ? _dataH.GetNodeByID(nodeId) : NULL);
		}
		if(m.size() != defaultM.size())
		{
			printf("AgentMgr::unserialize: ERROR: %u nodes in mapping, but should be %u!\n", (uint)m.size(), (uint)defaultM.size());
			return false;
		}
		Agent *a = new Agent(_dataH, m);
		if(!a->sanityCheck())
		{
			printf("WARNING: Failed to load Agent, mapping is incorrect or incompatible with current settings!\n");
			deleteAgent(a);
			return true; // ... because the line was executed correctly, anyway
		}
		a->locked = true;
		_lastUnserializedAgent = a;
		agents.push_back(a);
		return true;
	}
	else if(!strncmp(cmd.c_str(), "AGENT_", 6))
	{
		return _lastUnserializedAgent->unserialize(cmd, params);
	}

	return false;
}

Agent *AgentMgr::newAgent()
{
	// Revive & recycle no longer used ones -> less memory fragmentation
	if(_deadAgents.size())
	{
		Agent *ret = _deadAgents.back();
		ASSERT(ret);
		_deadAgents.pop_back();
		ret->resetLife();
		ret->locked = false;
		return ret;
	}

	return new Agent(_dataH, _dataH.GetDefaultMapping());
}

void AgentMgr::deleteAgent(Agent *a)
{
	ASSERT(a);
	ASSERT(std::find(_deadAgents.begin(), _deadAgents.end(), a) == _deadAgents.end());
	a->locked = true;
	_deadAgents.push_back(a);
}

void AgentMgr::Sort()
{
	std::sort(agents.begin(), agents.end(), AgentPtrCmp());
}

void AgentMgr::SortForDuplicateRemoval()
{
	std::sort(agents.begin(), agents.end(), AgentPtrHashCmp());
}

void AgentMgr::Compact()
{
	// If there's a bunch of NULL entries in the vector,
	// compact it. (Move all non-NULL to the beginning)
	size_t wpos = 0;
	size_t sz = agents.size();
	// Make wpos to point to first NULL entry. We know that everything before is non-NULL.
	while(wpos < sz && agents[wpos])
		++wpos;
	// Past the end, and no NULL? Done here.
	if(wpos == sz)
		return;
	// From there, start copying pointers down.
	// wpos will always point to NULL entries from there on,
	// because non-NULL entries further down in the vector will have been moved to
	// a valid wpos, leaving a NULL entry behind.
	for(size_t i = wpos; i < sz; ++i)
	{
		if(agents[i])
		{
			ASSERT(agents[wpos] == NULL);
			agents[wpos++] = agents[i];
			agents[i] = NULL;
		}
	}
#ifdef _DEBUG
	//printf("Compact: Removed %u entries\n", uint(sz - wpos));
#endif
	// Now wpos holds new vector size, and all NULL entries are at the end of the vector.
	agents.resize(wpos);
	// Now the vector has no more NULL entries.

	AssertCompacted();

	// To keep memory usage down and prevent too many dead agents piling up,
	// delete one from spare memory.
	if(_deadAgents.size())
	{
		delete _deadAgents.back();
		_deadAgents.pop_back();
	}
}

//useful if agents converge
//use it rarely to save performance
//assume that agents are sorted!!1!
void AgentMgr::DropDuplicates()
{
	//uint s = agents.size();	//debug
	uint dropped = 0;
	//uint dropInd = 0;

	//how it works:
	//agents with same mapping share the same score
	//implication: if agents are sorted, identical mapping are next to each other!
	// and the scores are equal, too.

	// Fast method
	// This starts at a position i and scans forward as long as the scores match, incrementing off.
	// If the scores mismatch, a new run is started.
	// WARNING: Requires calling AgentMgr::SortForDuplicateRemoval() before calling this
	uint off = 0;
	for(uint i=0; ; i += off)
	{
		if(i + 1 >= agents.size())
			break;
		off = 1;
		score refScore = agents[i]->getScore();
		score cmpScore = agents[i+1]->getScore();

		while(refScore == cmpScore)
		{
			if(agents[i]->getMapping() == agents[i+off]->getMapping())
			{
					deleteAgent(agents[i+off]);
					agents[i+off] = NULL;
					++dropped;
			}
			++off;

			if(i + off >= agents.size())
				break;

			cmpScore = agents[i+off]->getScore();
		}
	}

	//slow method (reliable; gets all duplicates!)
	// -- oh my, just no. NO.
	/*for(uint i=0; i<agents.size()-1; ++i)
	{
		if(!agents[i])
			continue;

		for(uint j=i+1; j<agents.size(); ++j)
		{
			if(!agents[j])
				continue;

			if(agents[i]->getScore() != agents[j]->getScore())
				break;

			if(agents[i]->getMapping() == agents[j]->getMapping())
			{
				deleteAgent(agents[j]);
				agents[j] = NULL;
				++dropped;
			}
		}
	}*/

	//dont continue if nothing has been deleted
	if(!dropped)
		return;

	//printf("Dropped %u duplicates\n", dropped);

	Compact();
}

void AgentMgr::AssertCompacted()
{
#ifndef NDEBUG
	bool good = true;
	for(size_t i = 0; i < agents.size(); ++i)
		if(!agents[i])
		{
			printf("ERROR: agents[%u of %u] == NULL\n", (uint)i, (uint)agents.size());
			good = false;
		}

	ASSERT(good);
#endif
}

