#ifndef THREADING_H
#define THREADING_H

#include <list>
#include <vector>
#include "MersenneTwister.h"
#include "UserSettings.h"


class Lockable
{
public:
	Lockable();
	virtual ~Lockable();
	void lock();
	void unlock();

protected:
	mutable void *_mtx;
};

class Waitable : public Lockable
{
public:
	Waitable();
	virtual ~Waitable();
	void wait(); // releases the associated lock while waiting
	void signal(); // signal a single waiting thread
	void broadcast(); // signal all waiting threads

protected:
	mutable void *_cond;
};

class MTGuard
{
public:
	MTGuard(Lockable& x) : _obj(&x) { x.lock(); }
	MTGuard(Lockable* x) : _obj(x)  { x->lock(); }
	~MTGuard() { _obj->unlock(); }
private:
	Lockable *_obj;
};



typedef void (*threadFunc)(MTRand&, void *, void*, void*);

#define DEF_THREADFUNC(func) static void func(MTRand& rng, void *x = NULL, void *y = NULL, void *z = NULL)


struct JobEntry
{
	threadFunc func;
	void *param;
	void *param2;
	void *param3;
};

typedef std::list<JobEntry> JobList;

class WorkerThread;

class ThreadPool
{
public:

	ThreadPool();
	~ThreadPool();

	void start(size_t workers = 0);

	void quit(bool wait = true);
	bool addJob(threadFunc f, void *param = NULL, void *param2 = NULL, void *param3 = NULL);
	void waitUntilEmpty();

	inline bool quitting() const { return _quit; }
	bool isIdle();
	void assertIdle();

	// callbacks used by worker threads
	inline void _onJobEnd();
	inline void _fetchJob(JobEntry& e);

	inline void lock() { _lock.lock(); }
	inline void unlock() { _lock.unlock(); }
	bool addJobUnsafe(threadFunc f, void *param = NULL, void *param2 = NULL, void *param3 = NULL);
	//inline void broadcast() { _lock.broadcast(); } // tell worker threads to get going

protected:

	void _spawnWorkers(size_t count);

	JobList _jobs; // used for internal synch
	Waitable _lock;
	size_t _threadCount;
	volatile bool _quit;
	volatile int _busyThreads; // need lock
	std::vector<WorkerThread*> _th;
};



#endif
