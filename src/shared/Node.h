#ifndef NODE_H
#define NODE_H

#include "common.h"
#include <set>

class Node;

typedef std::set<Node*> NodeSet;

class Node
{
protected:

	Node();

public:

	virtual ~Node();

	// forbid copying
	Node(const Node&);

	// somehow connected
	inline bool isConnTo(const Node *n) const
	{
		return (nb.find(const_cast<Node*>(n)) != nb.end() );// || ((this == n) && (hasSelfLoop));
	}

	inline bool hasEdgeTo(const Node *n) const
	{
		return (outgoing.find(const_cast<Node*>(n)) != outgoing.end());// || ((this == n) && (hasSelfLoop));
	}

	inline bool hasEdgeFrom(const Node *n) const
	{
		return (incoming.find(const_cast<Node*>(n)) != incoming.end());// || ((this == n) && (hasSelfLoop));
	}

	inline size_t getDegree() const
	{
		return nb.size() + hasSelfLoop;
	}

	inline size_t getDegreeIncoming() const
	{
		return incoming.size() + hasSelfLoop;
	}

	inline size_t getDegreeOutgoing() const
	{
		return outgoing.size() + hasSelfLoop;
	}

	NodeSet outgoing; // this ---> other, and undirected
	NodeSet incoming; // this <--- other, and undirected
	NodeSet nb; // all neighbors
	int hasSelfLoop;
};





#endif
