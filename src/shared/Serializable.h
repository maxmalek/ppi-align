#ifndef SERIALIZABLE_H
#define SERIALIZABLE_H

#include "common.h"

class ProgramState;

class Serializable
{
public:
	virtual void serialize(ProgramState& state) = 0;
	virtual bool unserialize(const std::string& cmd, const std::string& params) = 0;
};

#endif
