#ifndef GED_SCORE_H
#define GED_SCORE_H

#include "ProteinNode.h"

class DataHolder;

// -- in G1 --
// 1: p1 -> my[]
// 2: p1 <- my[]
// -- in G2 --
// 4: p2 -> ref[]
// 8: p2 <- ref[]
// ----- Numbers at EOL indicate how often such an edge can be seen
// ----- when checking incoming & outgoing edges of 2 nodes.
// ----- This is important for adjusting the actual numbers after counting.
// ----- General rule: Substitute: x2, Undirected: x2
enum GEDCaseIndex
{
	GEDI_NONE = 0, // dummy entry, not actually used

	GEDI_MIN_EDGE = 1,
	                                      // Amount of times seen
	GEDI_REMOVE_DIRECTED_1 = 1,           // 1
	GEDI_REMOVE_DIRECTED_2 = 2,           // 1
	GEDI_REMOVE_UNDIRECTED = 3,           // 2
	GEDI_ADD_DIRECTED_1 = 4,              // 1
	GEDI_SUBST_DIRECTED_1 = 5,            // 2
	GEDI_FLIP_1 = 6,                      // 2
	GEDI_UNDIRECTED_TO_DIRECTED_1 = 7,    // 2
	GEDI_ADD_DIRECTED_2 = 8,              // 1
	GEDI_FLIP_2 = 9,                      // 2
	GEDI_SUBST_DIRECTED_2 = 10,           // 2
	GEDI_UNDIRECTED_TO_DIRECTED_2 = 11,   // 2
	GEDI_ADD_UNDIRECTED = 12,             // 2
	GEDI_DIRECTED_TO_UNDIRECTED_1 = 13,   // 2
	GEDI_DIRECTED_TO_UNDIRECTED_2 = 14,   // 2
	GEDI_SUBST_UNDIRECTED = 15,           // 4

	GEDI_REMOVE_SELF_LOOP = 16,           // 1 (?)
	GEDI_ADD_SELF_LOOP = 17,              // 1 (?)
	GEDI_SUBST_SELF_LOOP = 18,            // 1 (?)


	GEDI_MAX_EDGE = 19,
	GEDI_MIN_NODE = 19,

	GEDI_ADD_NODE = 19,
	GEDI_REMOVE_NODE = 20,

	GEDI_MAX_NODE = 21,

	
	GEDI_MAX = 21
};

// accessor helpers
#define GEACC_REMOVED(a)       (a[GEDI_REMOVE_DIRECTED_1] + a[GEDI_REMOVE_DIRECTED_2] + a[GEDI_REMOVE_UNDIRECTED] + 2*a[GEDI_REMOVE_SELF_LOOP])
#define GEACC_ADDED(a)         (a[GEDI_ADD_DIRECTED_1]    + a[GEDI_ADD_DIRECTED_2]    + a[GEDI_ADD_UNDIRECTED]    + 2*a[GEDI_ADD_SELF_LOOP])
#define GEACC_SUBST(a)         (a[GEDI_SUBST_DIRECTED_1]  + a[GEDI_SUBST_DIRECTED_2]  + a[GEDI_SUBST_UNDIRECTED]  + 2*a[GEDI_SUBST_SELF_LOOP])
#define GEACC_FLIP(a)          (a[GEDI_FLIP_1] + a[GEDI_FLIP_2])
#define GEACC_D_TO_U(a)        (a[GEDI_DIRECTED_TO_UNDIRECTED_1] + a[GEDI_DIRECTED_TO_UNDIRECTED_2])
#define GEACC_U_TO_D(a)        (a[GEDI_UNDIRECTED_TO_DIRECTED_1] + a[GEDI_UNDIRECTED_TO_DIRECTED_2])
#define GEACC_NODES_REMOVED(a) (a[GEDI_ADD_NODE])
#define GEACC_NODES_ADDED(a)   (a[GEDI_REMOVE_NODE])

// 1: variable, 2: reference
// each param must be either 0 or 1
#define GED_MASK(AtoB_1, BtoA_1, AtoB_2, BtoA_2) ((AtoB_1) | ((BtoA_1) << 1) | ((AtoB_2) << 2) | ((BtoA_2) << 3))


struct GEDInfo
{
	inline GEDInfo() { reset(); }

	inline void reset()
	{
		memset(this, 0, sizeof(*this));
	}

	inline score getContributionOfSelfLoops(const score *scorev){
		score accu = 0;

		accu += scorev[GEDI_REMOVE_SELF_LOOP] * _c[GEDI_REMOVE_SELF_LOOP];
		accu += scorev[GEDI_ADD_SELF_LOOP]    * _c[GEDI_ADD_SELF_LOOP];
		accu += scorev[GEDI_SUBST_SELF_LOOP]  * _c[GEDI_SUBST_SELF_LOOP];

		return accu;
	}

	inline score evaluate(const score *scorev) const
	{
		score accu = 0;
		for(uint i = GEDI_MIN_EDGE; i < GEDI_MAX; ++i) // Leave out GEDI_NONE, it's not interesting and never used.
			accu += scorev[i] * _c[i];
		
		// !!! No devision by 2 anymore;
		// A mismatched edge is seen twice:
		// Counted as deleted in G1, and as added in G2.
		// FIXME: Not sure about the other scores; maybe just dividing by 2 isn't safe...?
		// Hmm... flipped is also seen twice.
		// And direction changed too.
		// SHOULD be safe. Still not 100% sure.
		return accu; 
	}

	inline bool operator==(const GEDInfo& o)
	{
		// Ignoring the first entry is intentional. See above.
		for(uint i = GEDI_MIN_EDGE; i < GEDI_MAX; ++i)
			if(_c[i] != o._c[i])
				return false;
		return true;
	}

	inline bool operator!=(const GEDInfo& o)
	{
		return !(*this == o);
	}

	inline uint& operator[](size_t i) { return _c[i]; }
	inline const uint operator[](size_t i) const { return _c[i]; }

	uint _c[GEDI_MAX];
};

// slow
void calcPairGED(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged);
void calcWholeGED(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, GEDInfo& ged);

// very fast, but needs index
void calcPairGEDIndexed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged, const uint *index);
void calcWholeGEDIndexed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, GEDInfo& ged, const uint *index);

void GEDPopulateIndex(const NodeMapping& my, uint *index);


#endif

