#include "ProteinHolder.h"
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cmath>
#include "tools.h"
#include "ProgressBar.h"
#include "UserSettings.h"


ProteinHolder::ProteinHolder(const UserSettings& settings)
	: _wantBLASTMatrix(false), _wantGraphletSigs(false), _settings(settings), initBlastValue(score(-1.0)), defaultBlastValue(score(-1))
{
}

ProteinHolder::~ProteinHolder()
{
	for(size_t i = 0; i < 2; ++i)
	{
		for(PNodeMap::iterator it = _grpN[i].begin(); it != _grpN[i].end(); ++it)
			delete it->second;
		for(ProtoMap::iterator it = _protos[i].begin(); it != _protos[i].end(); ++it)
			delete it->second;
	}
}

void ProteinHolder::SetGroupNames(const std::string& a, const std::string& b)
{
	_grpNames[0] = a;
	_grpNames[1] = b;
}

NodePrototype *ProteinHolder::_GetProto(const std::string& name, uint group)
{
	ProtoMap::const_iterator it = _protos[group].find(name);
	if(it != _protos[group].end())
		return it->second;

	NodePrototype *proto = new NodePrototype;
	proto->name = name;
	//strToLower(proto->name);
	_protos[group][proto->name] = proto;
	return proto;
}

ProteinNode *ProteinHolder::Get(uint group, const std::string& name_, bool create /* = false */)
{
	ASSERT(group <= 1);
	PNodeMap& m = _grpN[group];
	std::string name = name_;
	//strToLower(name);
	PNodeMap::iterator it = m.find(name);
	if(it != m.end())
		return it->second;

	if(create)
	{
		if(const NodePrototype *proto = _GetProto(name, group))
		{
			ProteinNode *p = new ProteinNode();
			p->proto = proto;
			p->group = group;

			_grpN[group][name] = p;

			return p;
		}
		else
			printf("Get+Create node: No prototype for %s\n", name.c_str());
	}

	return NULL;
}

bool ProteinHolder::ReadBLASTMatrix(const std::string& fn, bool useBinary /* = true */)
{
	_wantBLASTMatrix = true;
	
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadBLASTMatrix ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	// number of nodes in each group
	uint h = 0;
	uint w = 0;

	uint g;
	ProteinNode *p = NULL;

	f >> h >> w;

	printf("ReadBLASTMatrix: w = %u, h = %u\n", w, h);

	if(!h || !w)
	{
		f.close();
		return false;
	}

	uint fails = 0;
	uint good = 0;
	
	uint total = h + w;
	std::string name;
	
	for(uint i = 0; i < total; ++i)
	{
		f >> g;
		--g; // Assume 1 or 2, so this gets 0 or 1
		f >> name;
		fixProteinName(name);
		
		p = Get(g, name, true);
		_grp[g].push_back(p);

		if(p)
		{
			++good;
			p->blastMtxId = (uint)_grp[g].size() - 1; // there we just inserted
		}
		else
			++fails;
	}

	printf("ReadBLASTMatrix: %u good, %u fails\n", good, fails);


	printf("ReadBLASTMatrix: Loading Matrix (%u entries)...\n", w * h);

	if(!(useBinary && _ReadBLASTMatrixBin(fn + ".bin")))
	{
		_blast.resize(w, h);
		printf("ReadBLASTMatrix: Reading text (may take a while)...\n");
		ProgressBar bar(h);
		// slow for large files
		std::string tmp;
		for(uint y = 0; y < h; ++y)
		{
start:
			if(!std::getline(f, tmp))
			{
				printf("ReadBLASTMatrix - not enough lines (%u of %u)\n", y-1, h);
				f.close();
				return false;
			}
			chompNewline(tmp);
			// HACK - at end of line? getline() will return nothing then. Need to realign.
			if(!y && tmp.empty())
				goto start;
			std::istringstream is(tmp);
			for(uint x = 0; x < w; ++x)
			{
				is >> _blast(x, y);
				if(!is)
				{
					printf("ReadBLASTMatrix - matrix line %u too short (%u of %u)\n", y, x, w);
					f.close();
					return false;
				}
			}
			bar.Step();
		}
		
		if(useBinary)
			_WriteBLASTMatrixBin(fn + ".bin");
	}

	f.close();

	printf("ReadBLASTMatrix: ... Done!\n");

	return !fails;
}

int ProteinHolder::getIndex(uint g, std::string name, uint & good, uint & fails, ProteinNode * p){
	PNodeArray::iterator it;
	int z;

	fixProteinName(name); /* */ 
	/* TODO: FIXME */           
	/* we need to have TRUE? */ 
	p = Get(g, name, false);     
	if(p) /* if p is not NULL */
	{                           

		it = std::find(_grp[g].begin(), _grp[g].end(), p); 

		if (it == _grp[g].end()){                     
			z = _grp[g].size();

			++good;                         
			_grp[g].push_back(p);                     
			/* there we just inserted */              
			p->blastMtxId = (uint)_grp[g].size() - 1; 


		} else {
			z = std::distance(_grp[g].begin(), it);
		}

	} else {
		++fails;
		z = _grp[g].size();
	}

	return z;

}


bool ProteinHolder::ReadBLASTPairList(const std::string& fn, int left_grop, bool useBinary /* = true */)
{
	ASSERT(left_grop <= 1);

	uint fails = 0;
	uint good = 0;
	uint h, w;


	_wantBLASTMatrix = true;
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadBLASTPairList ERROR: File not found: %s\n", fn.c_str());
		return false;
	}
	
	
	h = _grpN[0].size();
	w = _grpN[1].size();

	printf("ReadBLASTPairList: Loading matrix (at most %d unique entries for the two networks)...\n", h*w);

	if(!(useBinary && _ReadBLASTMatrixBin(fn + ".bin")))
	{

		/* we will read only those pair which can be found in the input networks */
		_blast.resize(w, h); /* width and then h*/
		_blast.fill(initBlastValue);

		printf("ReadBLASTPairList: Reading text (may take a while)...\n");
		ProgressBar bar(h*w);

		score value;
		uint unique_pairs = 0;

		std::string name;
		ProteinNode *p = NULL;
		uint d1, d2;
		std::string name1;
		std::string name2;
			
		while (!(f.eof()) ){
			if (left_grop == 0){
				f >> name1;
				f >> name2;
			} else {
				f >> name2;
				f >> name1;
			}

			/* get index of protein by name, will skip those which are not in the input networks */
			 d1 = uint(getIndex(0, name1, good, fails, p));
			 d2 = uint(getIndex(1, name2, good, fails, p));


			// if both nodes are in the list of proteins
			if ((d1 <  _grp[0].size()) && (d2 <  _grp[1].size())) { 
				

				f >> value;
				uint y = d1;
				uint x = d2;
				
				
				if (_blast(x, y) == initBlastValue){
					bar.Step();
					++unique_pairs;
				}

				_blast(x, y) = value;

			} else {
				// a node is not in the blast similarity pairs list, see comment above
			}
				
		} // end of while
		
		printf("\nReadBLASTPairList: Finished reading pairs...\n");

		if (unique_pairs < h*w){
			printf("ReadBLASTPairList: Filling remaining %u pairs with defaultBlastValue=%lf ...\n", h*w - unique_pairs, defaultBlastValue);


			PNodeMap::iterator it;
			for(uint g=0; g <=1; g++){
				for(it=_grpN[g].begin(); it != _grpN[g].end(); ++it){
					d1 = uint(getIndex(g, it->first, good, fails, p));
				}
			}



			for(uint y = 0; y < _blast.getH(); ++y){
				for(uint x = 0; x < _blast.getW(); ++x){
					if (_blast(x, y) == initBlastValue){
			
						_blast(x, y) = defaultBlastValue;
						bar.Step();
					}
				} // by y
			} // by x
		
		}

		if(useBinary)
			_WriteBLASTMatrixBin(fn + ".bin");
	} // end of if

	f.close();

	printf("ReadBLASTPairList: ... Done!\n");

	return true;
}


bool ProteinHolder::ReadGraphletSigs(const std::string& fn, uint group)
{
	_wantGraphletSigs = true;
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadGraphletSigs ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	NodePrototype *proto = NULL;
	NodePrototype dummy;
	std::string line, name, cruft;
	uint c = 0;
	uint linecount = 0;

	while(std::getline(f, line))
	{
		++linecount;
		chompNewline(line);
		if(line.empty())
			continue;
		std::istringstream is(line);
		is >> name;
		if(name.empty())
		{
			printf("ReadGraphletSigs ERROR: name empty?!\n");
			return false;
		}
		fixProteinName(name);

		proto = _GetProto(name, group);
		ASSERT(proto);
		if(proto->hasSigs)
		{
			proto = &dummy;
			printf("ReadGraphletSigs: Ignoring duplicate for %s\n", name.c_str());
		}
		else
			++c; // initing new one

		for(uint i = 0; i < GRAPHLET_SIGS; ++i)
		{
			is >> proto->graphlets[i];
			if(!is)
			{
				printf("ReadGraphletSigs(%s) Line %u: Expected more data at index %u\n", fn.c_str(), linecount, i);
				return false;
			}
		}

		// this is not a problem, just safer
		if(is >> cruft)
		{
			printf("ReadGraphletSigs(%s) Line %u: Excess data at end of line\n", fn.c_str(), linecount);
		}


		proto->hasSigs = true;
	}

	f.close();

	printf("ReadGraphletSigs: %s -> %u\n", fn.c_str(), c);

	return true;
}

bool ProteinHolder::_AddPairByName(uint groupIdx, const std::string& name1, const std::string& name2)
{
	// FIXME: really? Allow self loops?
	if(name1 == name2)
	{

		////printf("ReadNetwork: Skipping %s self-loop\n", name1.c_str());
		
		// self-loops will be handled as undirected edges
		ProteinNode *p1 = Get(groupIdx, name1, true);
	
		if(!p1)
		{
			printf("ReadNetwork: Failed to map %s <-> %s\n", name1.c_str(), name2.c_str());
			return false;
		}
		p1->hasSelfLoop = 1;

		////return false;
		return true;
	}

	ProteinNode *p1 = Get(groupIdx, name1, true);
	ProteinNode *p2 = Get(groupIdx, name2, true);

	if(!(p1 && p2))
	{
		printf("ReadNetwork: Failed to map %s <-> %s\n", name1.c_str(), name2.c_str());
		return false;
	}

	p1->nb.insert(p2);
	p2->nb.insert(p1);

	p1->outgoing.insert(p2);
	p2->incoming.insert(p1);

	if(_settings.forceUndirectedEdges)
	{
		p1->incoming.insert(p2);
		p2->outgoing.insert(p1);
	}

	return true;
}

bool ProteinHolder::ReadNetwork(const std::string& fn)
{
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadNetwork ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	std::string line, code, name1, name2;
	uint groupIdx;
	uint c = 0;

	// skip header
	std::getline(f, line);

	while(std::getline(f, line))
	{
		std::stringstream is(line);
		is >> code;
		groupIdx = GetGroupIdx(code);
		if(groupIdx > 1)
		{
			printf("ReadNetwork: Unknown group %s\n", code.c_str());
			continue;
		}

		is >> name1 >> name2;
		// rest is not interesting

		fixProteinName(name1);
		fixProteinName(name2);
		
		if(_AddPairByName(groupIdx, name1, name2))
			++c;
	}

	printf("ReadNetwork: %s -> %u connections\n", fn.c_str(), c);
	return true;
}

// Reads simple .sif files exported by cytoscape - for testing
bool ProteinHolder::ReadNetworkSIF(const std::string& fn, uint groupIdx)
{
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadNetworkSIF ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	std::string name1, name2, ty;
	uint c = 0;

	while(true)
	{
		f >> name1 >> ty >> name2;
		if(!f)
			break;

		fixProteinName(name1);
		fixProteinName(name2);

		strToLower(ty);

		if(ty == "undirectededge" || ty == "undirected" || ty == "u")
		{
			undirected:
			if(_AddPairByName(groupIdx, name1, name2)
			&& _AddPairByName(groupIdx, name2, name1))
				++c;
		}
		else if(ty == "directededge" || ty == "directed" || ty == "d")
		{
			if(_AddPairByName(groupIdx, name1, name2))
				++c;
		}
		else
		{
			printf("ReadNetworkSIF: WARNING: Unknown interaction type '%s', assuming undirected!\n", ty.c_str());
			goto undirected;
		}
	}

	printf("ReadNetworkSIF: %s -> %u connections (group %u)\n", fn.c_str(), c, groupIdx);
	return true;
}

// Reads simple .edgelist files
bool ProteinHolder::ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader)
{
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadNetworkEDGELIST ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	std::string name1, name2;
	uint c = 0;

	if (hasHeader) f >> name1 >> name2;

	while(true)
	{
		f >> name1 >> name2;
		if(!f)
			break;

		fixProteinName(name1);
		fixProteinName(name2);

		if(_AddPairByName(groupIdx, name1, name2)) ++c;
	}

	printf("ReadNetworkEDGELIST: %s -> %u connections (group %u)\n", fn.c_str(), c, groupIdx);
	return true;
}


bool ProteinHolder::Prepare()
{
	uint fails = 0;

	printf("Group sizes: {name->Node} = [%u, %u]\n",  (uint)_grpN[0].size(), (uint)_grpN[1].size());

	if(!(_grpN[0].size() && _grpN[1].size()))
	{
		printf("ERROR: At least one network is empty. Did the files load correctly?\n");
		return false;
	}

	printf("Raw BLAST matrix size: %u\n", (uint)(_blast.getW() * _blast.getH()));
	printf("Raw BLAST matrix group sizes: [%u, %u]\n", (uint)_grp[0].size(), (uint)_grp[1].size());

	if(_wantBLASTMatrix)
	{
		/* TODO: FIXME -- is there everything correct? */
		if(_grp[0].size() != _grpN[0].size() || _grp[1].size() != _grpN[1].size())
		{
			printf("Prepare: Different amount of BLAST matrix entries and proteins!\n");
			return false;
		}
		// Note: BLAST matrix is stored rotated here

		if(_grp[0].size() != _blast.getH() || _grp[1].size() != _blast.getW())
		{
			printf("Prepare: BLAST matrix has incorrect size!\n");
			return false;
		}
	}

	_SetupNodeIDs();

	for(uint g = 0; g < 2; ++g)
		for(PNodeMap::iterator it = _grpN[g].begin(); it != _grpN[g].end(); ++it)
		{
			ProteinNode *p = it->second;
			if(!(p->proto
				&& p->id
				&& p->group <= 1
				&& p->group == g
				&& (!_wantGraphletSigs || p->proto->hasSigs)
				&& (!_wantBLASTMatrix || (p->blastMtxId != uint(-1) && _grp[g][p->blastMtxId] == p))
				&& p->name().length()))
			{
				++fails;
				printf("Prepare: Something is wrong with %s\n", it->first.c_str());
			}
		}

	if(fails)
	{
		printf("Prepare: %u ERRORS!\n", fails);
		return false;
	}

	printf("Prepare: Everything good!\n");

	if(_wantGraphletSigs)
		_PrecalcGraphletLogs();

	_PrecalcDegreeLogs();

	_SetupConnectivityMatrix(0);
	_SetupConnectivityMatrix(1);

	return true;
}

void ProteinHolder::_SetupConnectivityMatrix(uint g)
{
	printf("Setting up connectivity matrix for group %u ...\n", g);
	PNodeMap& M = _grpN[g];
	Matrix<uchar>& C = _conn[g];
	uint sz = M.size();

	uint id = 0;
	PNodeMap::iterator a, b;

	// Because calculating a large matrix in debug mode takes ages...
	if(_ReadConnectivityMatrixBin(g))
		return;

	C.resize(sz + 1, sz + 1); // leave space for null row/col

	ProgressBar bar(sz);

	// Fill null entries (first row and column)
	for(uint i = 0; i < sz + 1; ++i)
	{
		C(0, i) = 0;
		C(i, 0) = 0;
	}

	for(a = M.begin(); a != M.end(); ++a)
	{
		ASSERT(a->second->id);
		for(b = M.begin(); b != M.end(); ++b)
			C(a->second->id, b->second->id) = a->second->hasEdgeTo(b->second) ? 1 : 0;
		bar.Step();
	}

	_WriteConnectivityMatrixBin(g);
}

void ProteinHolder::_PrecalcGraphletLogs()
{
	printf("Precalculating graphlet logs...\n");
	for(uint j = 0; j < 2; ++j)
	{
		for (ProtoMap::iterator it = _protos[j].begin(); it != _protos[j].end(); ++it)
		{
			NodePrototype *p = it->second;
			for (uint i = 0; i < GRAPHLET_SIGS; ++i)
				p->graphletLogs[i] = log(score(p->graphlets[i] + 1)); // see GraphletScore.cpp
		}
	}
}

void ProteinHolder::_PrecalcDegreeLogs()
{
	printf("Precalculating graphlet logs...\n");
	for(uint j = 0; j < 2; ++j)
	{
		for (uint i = 0; i < _grpV[j].size(); ++i)
		{
			ProteinNode *p = _grpV[j][i];
			NodePrototype *proto = const_cast<NodePrototype*>(p->proto); // HACK

			// See NodeDistance.cpp
			proto->degreeLogIn  = log(score(p->getDegreeIncoming() + 1));
			proto->degreeLogOut = log(score(p->getDegreeOutgoing() + 1));
		}
	}
}

void ProteinHolder::Cleanup()
{
	_conn[0].forceDeallocate();
	_conn[1].forceDeallocate();
	_blast.forceDeallocate();

	PNodeArray().swap(_grp[0]);
	PNodeArray().swap(_grp[1]);
}

void ProteinHolder::_SetupNodeIDs()
{
	ASSERT(_grpV[0].empty());
	ASSERT(_grpV[1].empty());

	const PNodeMap& A = _grpN[0];
	const PNodeMap& B = _grpN[1];
	PNodeArray& AV = _grpV[0];
	PNodeArray& BV = _grpV[1];
	uint total = 0;

	// First pass: See if any nodes can be prematched
	if(_settings.matchSameNames)
	{
		// We're looking at group 0 ...
		for(PNodeMap::const_iterator it = A.begin(); it != A.end(); ++it)
		{
			ProteinNode *a = it->second;
			ProteinNode *b = Get(1, a->name()); // so search the match in group 1.
			if(b)
			{
				printf("Prematched nodes with name: %s\n", a->name().c_str());
				a->prematched = true;
				b->prematched = true;
				AV.push_back(a);
				BV.push_back(b);
				++total;
			}
		}
	}

	printf("Total nodes prematched: %u\n", total);

	// Second pass: Fill vectors
	for(PNodeMap::const_iterator it = A.begin(); it != A.end(); ++it)
		if(!it->second->prematched)
			AV.push_back(it->second);

	for(PNodeMap::const_iterator it = B.begin(); it != B.end(); ++it)
		if(!it->second->prematched)
			BV.push_back(it->second);

	// Assign IDs by position in vector. IMPORTANT: 0 is reserved for "invalid node"
	for(size_t i = 0; i < AV.size(); ++i)
		AV[i]->id = i + 1;
	for(size_t i = 0; i < BV.size(); ++i)
		BV[i]->id = i + 1;
}
