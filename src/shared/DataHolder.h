#ifndef DATA_HOLDER_H
#define DATA_HOLDER_H

// -- DataHolder --
// Precalculates data used by AgentMgr & co,
// or brings data (matrices) into a shape that accessing them is a lot faster.
// This, after initialization, is a pure read-only class.

#include "common.h"
#include "Matrix.h"
#include "ProteinNode.h"
#include "UserSettings.h"
#include "GEDScore.h"

class ProteinHolder;
class ThreadPool;

// IMPORTANT: variable is always on X axis! (and static group on Y axis)

class DataHolder
{
public:
	DataHolder(const UserSettings& settings);
	~DataHolder();

	void SetProteinHolder(ProteinHolder *ph) { _ph = ph; }
	void SetThreadPool(ThreadPool *th) { _thpool = th; }

	inline const UserSettings& GetUserSettings() const { return _settings; }

	bool Precalc();

	// Commented out because the BLAST matrix is cleared in DataHolder::_CalcCombinedScores().
	// If the BLAST score is needed externally, make sure the matrix isn't getting cleared,
	// and re-enable the code below.
	/*inline score GetBlastScore(uint a, uint b) const
	{
		return _lookupBLASTScore(a, b);
	}*/

	inline score GetGraphletScore(uint a, uint b) const
	{
		return _lookupGraphletScore(a, b);
	}

	inline score GetNodeDistScore(uint a, uint b) const
	{
		return _lookupNodeDistanceScore(a, b);
	}

	inline score GetSeqSimScore(uint a, uint b) const
	{
		return _lookupSeqSimScore(a, b);
	}

	// BLAST score if below threshold, mixed scores otherwise. See UserSettings.
	inline score GetCombinedScore(uint a, uint b) const
	{
		return _lookupCombinedScore(a, b);
	}

	inline char IsConnected(uint group, uint a, uint b) const
	{
		return _lookupConnRow[group](a, b);
	}

	// for sequential access
	inline const uchar *GetConnectionsRow(uint group, uint a) const
	{
		return _lookupConnRow[group].getRow(a);
	}
	inline const uchar *GetConnectionsCol(uint group, uint a) const
	{
		return _lookupConnCol[group].getColumn(a);
	}
	// inverted data (data XOR 1)
	inline const uchar *GetConnectionsRowInv(uint group, uint a) const
	{
		return _lookupConnRowInv[group].getRow(a);
	}
	inline const uchar *GetConnectionsColInv(uint group, uint a) const
	{
		return _lookupConnColInv[group].getColumn(a);
	}

	inline uint GetMaxVariableNodeID() const  { return _nodeMaxID[_settings.varGroup]; }
	inline uint GetMaxReferenceNodeID() const { return _nodeMaxID[!_settings.varGroup]; }

	inline uint GetMaxTotalNodeDegree() const { return _nodeMaxDegreeTotal; }
	inline uint GetMaxVariableNodeDegree() const  { return _nodeMaxDegree[_settings.varGroup]; }
	inline uint GetMaxReferenceNodeDegree() const { return _nodeMaxDegree[!_settings.varGroup]; }

	inline uint GetVariableEdgeAmount() const { return _totalEdgeAmount[_settings.varGroup]; }
	inline uint GetReferenceEdgeAmount() const { return _totalEdgeAmount[!_settings.varGroup]; }
	inline uint GetEdgeAmount(uint g) const { return _totalEdgeAmount[g]; }

	inline const NodeMapping& GetRefMapping() const { return _refMapping; }
	inline const NodeMapping& GetDefaultMapping() const { return _defaultMapping; }

	// Do NOT call this with 0 !
	inline const ProteinNode *GetNodeByID(uint id) const
	{
		ASSERT(id && id < _defaultMapping.size());
		return _defaultMapping.node[id - 1];
	}

	// equal to total edge amount in both graphs
	inline uint getWorstGEDRaw() const { return _worstGEDRaw; }
	inline score getWorstGEDScore() const { return _worstGEDScore; }

	bool HasDirectedEdges() const { return _hasDirectedEdges; }

	inline const score *getGEDScoreArray() const { return &_gedScores[0]; }

	// This is also the amount of prematched nodes that exist.
	inline uint getFirstNonPrematchedIndex() const { return _firstNonPrematchedIndex; }

	inline bool IsInitialized() const { return _wasInit; }

protected:

	void _SetupGraphletMatrix();
	void _PrecalcGraphletScores(Matrix<score>& m);
	void _SetupNodeDistanceMatrix();
	void _TransferBLASTScores();
	void _FillNeighborVector(uint group);
	void _TransferConnectionMatrix(uint group);
	void _GenerateDefaultMappings();
	//void _CalcWorstGED();
	void _CalcMisc(); // This figures out max node IDs and degree.
	void _CalcCombinedScores();
	void _FillGEDScoreArray();
	bool _CheckPrematch(uint g);
	bool _SetupSEQSIMMatrix();
	bool _ReadSEQSIMMatrixText(const std::string& fn, uint leftgroup, Matrix<score>& M); // needs to be in DataHolder because it needs valid node IDs

	bool _hasDirectedEdges;
	uint _firstNonPrematchedIndex;

	FastMatrix<score> _lookupCombinedScore; // BLAST score if below threshold, graphlet & node dist. score otherwise
	FastMatrix<score> _lookupGraphletScore;
	FastMatrix<score> _lookupNodeDistanceScore;
	FastMatrix<score> _lookupBLASTScore;
	FastMatrix<score> _lookupSeqSimScore;

	// row-wise
	FastMatrix<uchar> _lookupConnRow[2];
	FastMatrix<uchar> _lookupConnRowInv[2];

	// same data, but column-wise memory layout
	FastMatrixColumnWise<uchar> _lookupConnCol[2];
	FastMatrixColumnWise<uchar> _lookupConnColInv[2];

	uint _nodeMaxID[2];
	uint _nodeMaxDegree[2];
	uint _totalEdgeAmount[2];

	uint _nodeMaxDegreeTotal;

	score _gedScores[GEDI_MAX];


	ProteinHolder *_ph;

	// Both initially filled, padded to same length, and then left untouched. NULLs only at end of array.
	NodeMapping _refMapping;
	NodeMapping _defaultMapping;

	// Precalculated once, later used to scale GED score into a [0...1] range
	uint _worstGEDRaw;
	score _worstGEDScore;

	const UserSettings& _settings;

	ThreadPool *_thpool; // optional
	bool _wasInit;
};

#endif
