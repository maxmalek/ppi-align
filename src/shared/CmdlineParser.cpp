#include "CmdlineParser.h"
#include "ProgramState.h"

typedef int (*CfgFunc)(ProgramState&, const char * const *);

struct CmdlineEntry
{
	const char *opt;
	CfgFunc func;
	int params;
	int pass;
};

// returns how many params have been used, -1 on error
static int cmdNoWorkfiles(ProgramState& state, const char * const * args);
static int cmdNoSave(ProgramState& state, const char * const * args);
static int cmdMaxiter(ProgramState& state, const char * const * args);
static int cmdMaxsecs(ProgramState& state, const char * const * args);
static int cmdMaxsame(ProgramState& state, const char * const * args);
static int cmdLogfile(ProgramState& state, const char * const * args);
static int cmdLogiter(ProgramState& state, const char * const * args);
static int cmdThreads(ProgramState& state, const char * const * args);
static int cmdConfig(ProgramState& state, const char * const * args);
static int cmdAutosave(ProgramState& state, const char * const * args);
static int cmdSave(ProgramState& state, const char * const * args);
static int cmdResume(ProgramState& state, const char * const * args);
static int cmdSeries(ProgramState& state, const char * const * args);

static int cmdGroups(ProgramState& state, const char * const * args);

static int cmdNtw(ProgramState& state, const char * const * args);
static int cmdSif(ProgramState& state, const char * const * args);
static int cmdEdgeList(ProgramState& state, const char * const * args);
static int cmdEdgeListHeader(ProgramState& state, const char * const * args);

static int cmdBioScore(ProgramState& state, const char * const * args);

static int cmdBlastMatrix(ProgramState& state, const char * const * args);
static int cmdBlastPairList(ProgramState& state, const char * const * args);
static int cmdSeqSim(ProgramState& state, const char * const * args);

static int cmdGrsig(ProgramState& state, const char * const * args);

static int cmdAcceptEmpty(ProgramState& state, const char * const * args);
static int cmdCreate(ProgramState& state, const char * const * args);
static int cmdPop(ProgramState& state, const char * const * args);
static int cmdUndirected(ProgramState& state, const char * const * args);
static int cmdNoPrematch(ProgramState& state, const char * const * args);
static int cmdOutPrefix(ProgramState& state, const char * const * args);

// Note: if you change this, update CmdHelp.cpp if necessary!
static CmdlineEntry cmdtab[] =
{
	{ "no-workfiles",   cmdNoWorkfiles,   0,    0 },
	{ "no-save",        cmdNoSave,        0,    0 },

	{ "accept-empty",   cmdAcceptEmpty,   0,    0 },
	{ "create",         cmdCreate,        0,    0 },
	{ "maxiter",        cmdMaxiter,       1,    0 },  // integer
	{ "maxsecs",        cmdMaxsecs,       1,    0 },  // integer
	{ "maxsame",        cmdMaxsame,       1,    0 },  // integer
	{ "logfile",        cmdLogfile,       1,    0 },  // filename
	{ "logiter",        cmdLogiter,       1,    0 },  // integer
	{ "threads",        cmdThreads,       1,    0 },  // numThreads
	{ "config",         cmdConfig,        1,    0 },
	{ "c",              cmdConfig,        1,    0 },
	{ "autosave",       cmdAutosave,      1,    0 },  // autosaveSecs
	{ "save",           cmdSave,          1,    0 },  // file.state
	{ "resume",         cmdResume,        1,    0 },  // file.state
	{ "series",         cmdSeries,        0,    0 },  // file.state

	{ "groups",         cmdGroups,        2,    0 },  // group_1         group_2

	{ "ntw",            cmdNtw,           1,    1 },  // file.ntw
	{ "sif",            cmdSif,           2,    1 },  // file.sif        group
	{ "edgelist",       cmdEdgeList,      2,    1 },  // file.edgelist   group
	{ "edgelisth",      cmdEdgeListHeader,2,    1 },  // file.edgelisth  group

	{ "bioscore",       cmdBioScore,      1,    1 },  // "S or E"

	{ "blast",          cmdBlastMatrix,   1,    1 },  // file.distmatrix
	{ "blastmatrix",    cmdBlastMatrix,   1,    1 },  // file.distmatrix
	{ "blastpairlist",  cmdBlastPairList, 2,    1 },  // file.distlist   group
	{ "seqsim",         cmdSeqSim,        2,    1 },  // file.seqsim     group

	{ "grsig",          cmdGrsig,         2,    1 },  // file.sigs       group

	// shortcuts
	{ "pop",            cmdPop,           1,    0 },  // integer
	{ "undirected",     cmdUndirected,    0,    0 },
	{ "no-prematch",    cmdNoPrematch,    0,    0 },

	{ "outprefix",      cmdOutPrefix,     1,    0 }, // string
	{ NULL, NULL }
};

const int MAX_PASS = 1; // max pass value in table above

CmdlineParser::CmdlineParser(int argc, char **argv)
	: argc(argc - 1)
	, argv(argv + 1)
{
}

bool CmdlineParser::Apply(ProgramState& state)
{
	for(int i = 0; i <= MAX_PASS; ++i)
		if(!_ApplyPass(state, i))
			return false;
	return true;
}

bool CmdlineParser::_ApplyPass(ProgramState& state, int pass)
{
	int idx = 0;
	while(idx < argc)
	{
		int result = _HandleEntry(idx, state, pass);
		if(result < 0)
		{
			printf("There were command line errors, bailing out...\n");
			return false;
		}
		else
			idx += result + 1;
	}
	return true;
}

int CmdlineParser::_HandleEntry(int idx, ProgramState& state, int pass)
{
	const char *e = argv[idx];

	if(!e)
		return -1;

	int paramsRemain = argc - idx - 1;
	if(paramsRemain < 0)
		return -1;

	if(*e != '-')
	{
		printf("Error: No context for \"%s\"\n", e);
		return -1;
	}

	// strip "-", "--", ... from option
	while(*e && *e == '-')
		++e;
	if(!*e)
	{
		printf("Error: Malformed command line: empty option\n");
		return -1;
	}

	// scan forward until first -param is found
	int paramsAvail = paramsRemain;
	for(int i = 1; i < paramsRemain; ++i)
		if(*argv[idx + i] == '-')
		{
			paramsAvail = i - 1;
			break;
		}

	// Look for handler in table
	for(uint i = 0; cmdtab[i].func; ++i)
	{
		if(!strcmp(e, cmdtab[i].opt))
		{
			if(paramsAvail >= cmdtab[i].params)
			{
				if(cmdtab[i].pass != pass)
					return cmdtab[i].params;

				printf("Option: %s\n", e);
				int res = cmdtab[i].func(state, &argv[idx+1]);
				if(res < 0)
					return res;

				return cmdtab[i].params;
			}
			else
			{
				printf("Error: Option \"%s\": Need %u params, got %u\n", e, cmdtab[i].params, paramsAvail);
				return -1;
			}
		}
	}

	printf("Error: Unknown option: \"%s\"\n", e);
	return -1;
}


static int cmdNoWorkfiles(ProgramState& state, const char * const * /*args*/)
{
	state.settings.noWorkfiles = 1;
	return 0;
}

static int cmdNoSave(ProgramState& state, const char * const * /*args*/)
{
	state.settings.noSave = 1;
	return 0;
}

static int cmdAcceptEmpty(ProgramState& state, const char * const * args)
{
	state.settings.acceptEmptyState = 1;
	return 0;
}

static int cmdCreate(ProgramState& state, const char * const * args)
{
	state.settings.createStateIfNotExist = 1;
	return 0;
}

static int cmdMaxiter(ProgramState& state, const char * const * args)
{
	state.settings.abort_iterations = atoi(args[0]);
	return 1;
}

static int cmdMaxsecs(ProgramState& state, const char * const * args)
{
	state.settings.abort_seconds = atoi(args[0]);
	return 1;
}

static int cmdMaxsame(ProgramState& state, const char * const * args)
{
	state.settings.abort_maxIterationsWithoutScoreChange = atoi(args[0]);
	return 1;
}

static int cmdLogfile(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	state.settings.logger_file = args[0];
	return 1;
}

static int cmdLogiter(ProgramState& state, const char * const * args)
{
	state.settings.logger_iterations = atoi(args[0]);
	return 1;
}

static int cmdThreads(ProgramState& state, const char * const * args)
{
	state.settings.numThreads = atoi(args[0]);
	return 1;
}

static int cmdConfig(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	if(!state.HandleCommandlineCmd(args[0]))
		return -1;

	return 1;
}

static int cmdResume(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	state.settings.loadStateName = args[0];

	if(state.settings.saveStateName.empty())
		state.settings.saveStateName = state.settings.loadStateName;

	return 1;
}

static int cmdSave(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	state.settings.saveStateName = args[0];
	return 1;
}

static int cmdAutosave(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	state.settings.autosaveSecs = atoi(args[0]);
	return 1;
}

static int cmdSeries(ProgramState& state, const char * const * args)
{
	state.settings.saveSeries = 1;
	return 0;
}

static int cmdGroups(ProgramState& state, const char * const * args)
{
	if(!*args[0] || !*args[1])
		return -1;

	state.SetGroupNames(args[0], args[1]);
	return 2;
}

static int cmdNtw(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	if(!state.ReadNetwork(args[0]))
		return -1;

	return 1;
}


static int cmdSif(ProgramState& state, const char * const * args)
{
	if(!*args[0] || !*args[1])
		return -1;

	const uint group = state.ph.GetGroupIdx(args[1]);
	if(group > 1)
	{
		printf("Error: option �sif�: Unknown group \"%s\"\n", args[1]);
		return -1;
	}
	if(!state.ReadNetworkSIF(args[0], group))
		return -1;

	return 2;
}

static int cmdEdgeList(ProgramState& state, const char * const * args)
{
	if(!*args[0] || !*args[1])
		return -1;

	const uint group = state.ph.GetGroupIdx(args[1]);
	if(group > 1)
	{
		printf("Error: option �sif�: Unknown group \"%s\"\n", args[1]);
		return -1;
	}
	if(!state.ReadNetworkEDGELIST(args[0], group, false))
		return -1;

	return 2;
}

static int cmdEdgeListHeader(ProgramState& state, const char * const * args)
{
	if(!*args[0] || !*args[1])
		return -1;

	const uint group = state.ph.GetGroupIdx(args[1]);
	if(group > 1)
	{
		printf("Error: option �sif�: Unknown group \"%s\"\n", args[1]);
		return -1;
	}
	if(!state.ReadNetworkEDGELIST(args[0], group, true))
		return -1;

	return 2;
}


static int cmdGrsig(ProgramState& state, const char * const * args)
{
	if(!*args[0] || !*args[1])
		return -1;

	const uint group = state.ph.GetGroupIdx(args[1]);
	if(group > 1)
	{
		printf("Error: option �grsig�: Unknown group \"%s\"\n", args[1]);
		return -1;
	}
	if(!state.ReadGraphletSigs(args[0], group))
		return -1;

	return 2;
}

static int cmdBioScore(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;

	if ( !((strcmp(args[0], "E") == 0) || (strcmp(args[0], "S") == 0)) ){
		printf("Error: option �bioscore�: bioScoreType has to be either \"E\" or \"S\" (not \"%s\"!\n", args[0]);
		return -1;
	}

	state.settings.bioScoreType = args[0];
	return 1;
}


static int CheckAndSetDefaultBioScoreType(ProgramState& state, std::string option_name, std::string function_name){

	if (state.settings.bioScoreType != "NULL"){

		if (state.settings.thresholdBLAST == -1.0){
			if ( !(state.settings.bioScoreType == "E") ){
				state.settings.thresholdBLAST = USER_SETTINGS_DEFAULT_BLAST_EVALUE;

			} else if ( !(state.settings.bioScoreType == "S") ){
				state.settings.thresholdBLAST = USER_SETTINGS_DEFAULT_BLAST_SSCORE;
			}
		}
	} else {
		printf("Error: option �%s� (in %s): bioScoreType (\"E\" or \"S\" using --bioscore) have to be specified before the reading blast values\n", option_name.c_str(), function_name.c_str());
		return -1;
	}
	return 1;
}

static int cmdBlastMatrix(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	std::string argName = args[-1];

	if (CheckAndSetDefaultBioScoreType(state, argName.substr(2), "cmdBlastMatrix") < 0){ return -1; }

	if(!state.ReadBLASTMatrix(args[0]))
		return -1;

	return 1;
}

static int cmdBlastPairList(ProgramState& state, const char * const * args)
{
	const uint group = state.ph.GetGroupIdx(args[1]);
	if(group > 1)
	{
		printf("Error: option �blastpairlist�: Unknown group \"%s\"\n", args[1]);
		return -1;
	}

	if (CheckAndSetDefaultBioScoreType(state, "blastpairlist", "cmdBlastPairList") < 0){ return -1; }

	if(!state.ReadBLASTPairList(args[0], group))
		return -1;

	return 1;
}

static int cmdSeqSim(ProgramState& state, const char * const * args)
{
	const uint group = state.ph.GetGroupIdx(args[1]);
	if(group > 1)
	{
		printf("Error: option �seqsim�: Unknown group \"%s\"\n", args[1]);
		return -1;
	}

	if(!state.ReadSeqSimData(args[0], group))
		return -1;
	return 0;
}



static int cmdPop(ProgramState& state, const char * const * args)
{
	state.settings.maxAgents = atoi(args[0]);
	return state.settings.maxAgents ? 1 : -1;
}

static int cmdUndirected(ProgramState& state, const char * const * /*args*/)
{
	state.settings.forceUndirectedEdges = 1;
	return 0;
}

static int cmdNoPrematch(ProgramState& state, const char * const * /*args*/)
{
	state.settings.matchSameNames = 0;
	return 0;
}


static int cmdOutPrefix(ProgramState& state, const char * const * args)
{
	state.settings.outPrefix = args[0];
	return 0;
}
