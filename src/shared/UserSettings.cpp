#include "UserSettings.h"
#include "ProgramState.h"
#include <sstream>
#include "tools.h"



UserSettings::UserSettings()
{
	/* double */    weightPairsum = 0;
	/* double */    weightGraphlets = 0;
	
	/* double */    weightGED = 1;
	/* double */    weightNodeDist = 0;
	/* double */    weightSeqSim = 0;

	/* double */    pairWeightGraphlets = 1; // automatically replaced by node dist if not present...
	/* double */    pairWeightBLAST = 1.0; /* introduced */
	/* string */    bioScoreType = "NULL"; /* introduced */

	/* double */    pairWeightGED = 1;
	/* double */    pairWeightNodeDist = 0; // ... that's why this is 0.
	/* double */    pairWeightSeqSim = 1;

	/* float */     basicHealth = 100;
	/* float */     maxHealthDrop = 100;

	// When mapping a valid node against NULL, this is returned for scores where no better value is available.
	// This value controls how "forcefully" the two graphs are aligned, in other words,
	// how high the penalty for mapping a node to NULL is.
	// A value of 1 is the worst possible, so the algorithm will try to prevent NULL<->node pairs at all costs.
	// Lower values (such as 0.85-0.99) will cause exceptionally bad pairs to be considered worse than mapping against NULL,
	// so that such nodes will be preferably counted as inserted or deleted.
	// It depends on your goal:
	// To minimize GED, this value should stay 1.
	// To maximize biological meaning, set this value lower.
	/* double */    pairNullValue = 1;

	// When loading network edge lists, automatically add missing edges
	// so that the final network will not have any directed edges.
	/* uint */      forceUndirectedEdges = 0;

	// When both networks to be worked on contain proteins with the same name,
	// assume they are the same proteins and should always be matched to each other.
	/* uint */      matchSameNames = 1;

	// -- For GED calculation/scoring. Graphs are G1 [varGroup] and G2 [!varGroup]

	// Penalty if an edge exists in G1, but not G2
	/* double */    edgeAddedGEDScore = 1;

	// Penalty if an edge exists in G2, but not G1
	/* double */    edgeRemovedGEDScore = 1;

	// Penalty if an edge exists in both G1 and G2 in exactly the same direction
	/* double */    edgeSubstitutedGEDScore = 0;

	// Penalty if a directed edge changed it's direction
	/* double */    edgeFlippedGEDScore = 0.8;

	// Penalty if an edge exists as directed edge in G1, but as undirected edge in G2
	/* double */    edgeDirectedToUndirectedGEDScore = 0.2;

	// Penalty if an edge exists as undirected edge in G1, but as directed edge in G2
	/* double */    edgeUndirectedToDirectedGEDScore = 0.2;

	// Penalty if a node exists in G2, but not G1
	/* double */    nodeAddedGEDScore = 0;

	// Penalty if a node exists in G1, but not G2
	/* double */    nodeRemovedGEDScore = 0;

	// -----------


	// Take the BLAST score only if it's below this threshold. Otherwise, use graphlet sig score.
	/* double */    thresholdBLAST = -1.0; // by default thresholdBLAST is either USER_SETTINGS_DEFAULT_BLAST_EVALUE 
	//                                           or USER_SETTINGS_DEFAULT_BLAST_SSCORE depending on bioScoreType; 
	//                                        if not specified thresholdBLAST is set to default in void CheckAndSetDefaultBioScoreType(...)

	// group ID that should be mapped to the variable group (0 or 1)
	/* uint */      varGroup = 0;

	// Rough, relative limit of Agents to spawn or keep. // TODO: clarify this
	/* uint */      maxAgents = 400;

	// Use greedy init to create the initial population?
	/* uint */      evo_greedyInitOnInit = 0;

	// Evo's greedy init will only consider scores strictly lower than this as a pair worth keeping,
	// and will otherwise map the node in question to NULL (ie. deletion or insertion).
	/* double */    evo_greedyInitScoreLimit = 0.4;  // In [0 .. 1]

	// This fraction of agents will be initialized using the greedy method.
	// The rest will be mutated normally.
	// Warning: maxAgents * evo_greedyInitPerRound must be >= 1, otherwise this has no effect
	/* double */    evo_greedyInitPerRound = 0;  // In [0 .. 1]

	// If this is true, calculate one whole island per thread, instead of one Agent per thread.
	// Only effective for the islands algorithm.
	/* uint/bool */ islands_coarseThreadGranularity = 0;

	// Each zone spans a score threshold of this value (i.e. if an island's score is too high
	// for it's current zone, it is moved into the next zone)
	/* float */     islands_zoneGranularity = 0.05f;

	// After how many iterations in one algo to switch to the next algo
	/* uint */      islands_algoIterations[ALGO_BEES] = 1000;
	/* uint */      islands_algoIterations[ALGO_EVO]  = 100;

	// After how many iterations an islands is allowed to change zone
	/* uint */      islands_minZoneIterations = 200;

	// How many islands to keep in the "breeder" zone (refill if necessary)
	/* uint */      islands_startZoneIslands = 40;

	// After how many world iterations to check each zone for islands to move to next zone
	/* uint */      islands_zoneCheckInterval = 30;

	// How many islands to keep per zone. If more islands exist some are merged
	// to reduce the total number. 0 to disable.
	/* uint */      islands_maxIslandsPerZone = 40;



	// Abort criteria - by default we don't abort
	abort_seconds = 0;
	abort_iterations = 0;
	abort_maxIterationsWithoutScoreChange = 0;

	// Misc settings
	numThreads = 0; // auto-detect unless specified otherwise
	noWorkfiles = 0; // by default create auxiliary files such as matrix binary dumps
	noSave = 0; // by default, save state on exit
	autosaveSecs = 0; // do not autosave by default
	logger_iterations = 10;
	acceptEmptyState = 0;
	createStateIfNotExist = 0;
	saveSeries = 0;

	// Internal stuff
	_finalCalcGED = true;
	_pairCalcGED = true;
	_finalCalcSeqSim = true;
	_finalCalcPairsum = true;
	_finalCalcNodeDist = true;
	_pairCalcSeqSim = true;
	_good = false;
};

#define WRITE_VAR(x) { std::ostringstream os; os << #x << " " << x; state.AddCmd("V", os.str(), true); }

void UserSettings::serialize(ProgramState& state)
{
	WRITE_VAR(weightPairsum);
	WRITE_VAR(weightGraphlets);
	WRITE_VAR(weightGED);
	WRITE_VAR(weightNodeDist);
	WRITE_VAR(weightSeqSim);

	WRITE_VAR(pairWeightGraphlets);
	WRITE_VAR(pairWeightGED);
	WRITE_VAR(pairWeightNodeDist);
	WRITE_VAR(pairWeightSeqSim);

	WRITE_VAR(pairNullValue);
	WRITE_VAR(thresholdBLAST);
	WRITE_VAR(forceUndirectedEdges);
	WRITE_VAR(matchSameNames);

	WRITE_VAR(edgeAddedGEDScore);
	WRITE_VAR(edgeRemovedGEDScore);
	WRITE_VAR(edgeSubstitutedGEDScore);
	WRITE_VAR(edgeFlippedGEDScore);
	WRITE_VAR(edgeDirectedToUndirectedGEDScore);
	WRITE_VAR(edgeUndirectedToDirectedGEDScore);
	WRITE_VAR(nodeAddedGEDScore);
	WRITE_VAR(nodeRemovedGEDScore);

	WRITE_VAR(basicHealth);
	WRITE_VAR(maxHealthDrop);
	WRITE_VAR(varGroup);
	WRITE_VAR(maxAgents);
	WRITE_VAR(evo_greedyInitScoreLimit);
	WRITE_VAR(evo_greedyInitPerRound);
	WRITE_VAR(evo_greedyInitOnInit);
	WRITE_VAR(islands_coarseThreadGranularity);
	WRITE_VAR(islands_algoIterations[ALGO_BEES]);
	WRITE_VAR(islands_algoIterations[ALGO_EVO]);
	WRITE_VAR(islands_zoneGranularity);
	WRITE_VAR(islands_minZoneIterations);
	WRITE_VAR(islands_startZoneIslands);
	WRITE_VAR(islands_zoneCheckInterval);
	WRITE_VAR(islands_maxIslandsPerZone);
}

// poor man's all-purpose conversion function
template<typename T> static bool from_str(const std::string& in, T& out)
{
	std::istringstream ss(in);
	if(!(ss >> out))
	{
		printf("WARNING: UserSettings failed to convert value '%s'\n", in.c_str());
		return false;
	}
	return true;
}

#define READ_VAR(x) if(var == #x) { return from_str(val, x); }

bool UserSettings::unserialize(const std::string& var, const std::string& val)
{
	READ_VAR(weightPairsum);
	READ_VAR(weightGraphlets);
	READ_VAR(weightGED);
	READ_VAR(weightNodeDist);
	READ_VAR(weightSeqSim);

	READ_VAR(pairWeightGraphlets);
	READ_VAR(pairWeightGED);
	READ_VAR(pairWeightNodeDist);
	READ_VAR(pairWeightSeqSim);

	READ_VAR(pairNullValue);
	READ_VAR(thresholdBLAST);
	READ_VAR(forceUndirectedEdges);
	READ_VAR(matchSameNames);

	READ_VAR(edgeAddedGEDScore);
	READ_VAR(edgeRemovedGEDScore);
	READ_VAR(edgeSubstitutedGEDScore);
	READ_VAR(edgeFlippedGEDScore);
	READ_VAR(edgeDirectedToUndirectedGEDScore);
	READ_VAR(edgeUndirectedToDirectedGEDScore);
	READ_VAR(nodeAddedGEDScore);
	READ_VAR(nodeRemovedGEDScore);

	READ_VAR(basicHealth);
	READ_VAR(maxHealthDrop);
	READ_VAR(varGroup);
	READ_VAR(maxAgents);
	READ_VAR(evo_greedyInitOnInit);
	READ_VAR(evo_greedyInitScoreLimit);
	READ_VAR(evo_greedyInitPerRound);
	READ_VAR(islands_coarseThreadGranularity);
	READ_VAR(islands_algoIterations[ALGO_BEES]);
	READ_VAR(islands_algoIterations[ALGO_EVO]);
	READ_VAR(islands_zoneGranularity);
	READ_VAR(islands_minZoneIterations);
	READ_VAR(islands_startZoneIslands);
	READ_VAR(islands_zoneCheckInterval);
	READ_VAR(islands_maxIslandsPerZone);

	// no match
	printf("UserSettings::unserialize(): Unknown variable %s = %s\n", var.c_str(), val.c_str());
	return false;
}

bool UserSettings::CheckAndApply()
{
	if(evo_greedyInitPerRound > 0 && evo_greedyInitPerRound * maxAgents < 1)
		printf("Settings: WARNING: (evo_greedyInitPerRound * maxAgents) < 1 -- greedy will not be used\n");

	if(!(varGroup == 0 || varGroup == 1))
	{
		printf("Settings: ERROR: varGroup is not 0 or 1\n");
		return false;
	}

	
	if(seqsimMatrixFile.empty())
	{
		weightSeqSim = 0;
		pairWeightSeqSim = 0;
	}
	

	if (blastScoreFile.empty()){
		pairWeightBLAST = 0;
	}


	_pairCalcGED = (pairWeightGED != 0);
	_pairScaler = pairWeightGED + pairWeightGraphlets + pairWeightNodeDist + pairWeightSeqSim + pairWeightBLAST;

	_finalCalcGED = (weightGED > 0);
	_finalCalcPairsum = (weightPairsum > 0);
	_finalCalcGraphlet = (weightGraphlets > 0);
	_finalCalcNodeDist = (weightNodeDist > 0);

	
	_finalCalcSeqSim = (weightSeqSim > 0);
	_pairCalcSeqSim  = (pairWeightSeqSim > 0);

	_good = true;
	return true;
}
