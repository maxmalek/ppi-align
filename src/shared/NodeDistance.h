#ifndef NODEDISTANCE_H
#define NODEDISTANCE_H

#include "common.h"
#include "ProteinNode.h"

score calcNodeDegreeDistance(const ProteinNode *a, const ProteinNode *b);

#endif // NODEDISTANCE_H
