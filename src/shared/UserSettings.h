#ifndef USERSETTINGS_H
#define USERSETTINGS_H

#include "Serializable.h"

#define USER_SETTINGS_DEFAULT_BLAST_EVALUE 0.000001
#define USER_SETTINGS_DEFAULT_BLAST_SSCORE 45


// When changing this, also adjust the switch in World.cpp - World::GetAlgo() !
enum AlgoID
{
	ALGO_BEES,
	ALGO_EVO,

	ALGO_MAX // needs to be last in list
};

// See cpp file for settings description and default values.
class UserSettings : public Serializable
{
public:

	UserSettings();

	virtual void serialize(ProgramState& state);
	virtual bool unserialize(const std::string& cmd, const std::string& params);
	bool CheckAndApply();

	// -- Temporary/runtime-generated settings --
	// (Intentionally moved to start of object to make sure it's in the CPU cache - accessed very often)
	bool _pairCalcGED;
	bool _pairCalcSeqSim;
	bool _finalCalcGED;
	bool _finalCalcGraphlet;
	bool _finalCalcPairsum;
	bool _finalCalcNodeDist;
	bool _finalCalcSeqSim;
	score _pairScaler;

	// ----- common settings -----

	// for global score
	score weightGraphlets;
	score weightPairsum;
	score weightGED;
	score weightSeqSim;
	score weightNodeDist;

	// for pairwise score
	score thresholdBLAST;
	score pairWeightGraphlets;

	std::string blastScoreFile; /* Introduced: need it as indicator; if it is NULL then reset pairWeightBLAST to 0, if */
	std::string bioScoreType;   /* Introduced */
	score pairWeightBLAST;      /* Introduced */
	score pairWeightGED;
	score pairWeightNodeDist;
	score pairWeightSeqSim;

	score pairNullValue;
	//uint pairBLASTReplace;

	float basicHealth;
	float maxHealthDrop;
	uint varGroup;
	uint forceUndirectedEdges;
	uint matchSameNames;
	std::string outPrefix;

	score edgeAddedGEDScore;
	score edgeRemovedGEDScore;
	score edgeSubstitutedGEDScore;
	score edgeFlippedGEDScore;
	score edgeDirectedToUndirectedGEDScore;
	score edgeUndirectedToDirectedGEDScore;
	score nodeAddedGEDScore;
	score nodeRemovedGEDScore;

	// for population
	uint maxAgents;

	// evo-specific settings
	uint  evo_greedyInitOnInit;
	score evo_greedyInitScoreLimit;
	score evo_greedyInitPerRound;


	// bees specific settings
	// TODO


	// islands specific settings
	uint islands_coarseThreadGranularity; // bool
	uint islands_algoIterations[ALGO_MAX];
	float islands_zoneGranularity;
	uint islands_minZoneIterations;
	uint islands_startZoneIslands;
	uint islands_zoneCheckInterval;
	uint islands_maxIslandsPerZone;


	// ProgramState settings -- NOT saved to state
	uint abort_seconds;
	uint abort_iterations;
	uint abort_maxIterationsWithoutScoreChange;

	// Misc settings -- NOT saved to state
	uint numThreads;
	uint noWorkfiles;
	uint noSave;
	uint autosaveSecs;
	std::string loadStateName;
	std::string saveStateName;
	std::string logger_file;
	uint logger_iterations;
	uint acceptEmptyState;
	uint createStateIfNotExist;
	uint saveSeries;

	// Must be delay loaded
	std::string seqsimMatrixFile;
	uint seqsimMatrixLeftgroup;


	// Set to true if checked and applied
	bool _good;


	inline score getPairWeightSum() const
	{
		return _pairScaler; // see UserSettings.cpp
	}
	inline score getFinalWeightSum() const
	{
		return weightPairsum + weightGED + weightGraphlets + weightNodeDist;
	}
};

#endif
