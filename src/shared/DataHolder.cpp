#include "common.h"
#include <algorithm>
#include <fstream>
#include <sstream>
#include "DataHolder.h"
#include "ProteinHolder.h"
#include "GEDScore.h"
#include "GraphletScore.h"
#include "ProgressBar.h"
#include "tools.h"
#include "GEDScore.h"
#include "UserSettings.h"
#include "Threading.h"
#include "NodeDistance.h"


DataHolder::DataHolder(const UserSettings& settings)
: _ph(NULL), _settings(settings), _thpool(NULL), _wasInit(false)
{
}

DataHolder::~DataHolder()
{
}

bool DataHolder::Precalc()
{
	if(!_settings._good)
	{
		printf("DataHolder: ERROR: UserSetting::CheckAndApply() not called!");
		return false;
	}

	ASSERT(_ph);
	ASSERT(_settings.varGroup == 0 || _settings.varGroup == 1);

	if(!_SetupSEQSIMMatrix()) // does nothing if no matrix specified or not used
		return false;

	_TransferBLASTScores();

	_TransferConnectionMatrix(0);
	_TransferConnectionMatrix(1);

	// Cleanup after the to-be-dropped matrix data were used,
	// but before allocating the other (large) matrices,
	// to avoid memory usage spikes as good as possible.
	_ph->Cleanup();

	_FillNeighborVector(0);
	_FillNeighborVector(1);

	_FillGEDScoreArray();
	_CalcMisc();
	_SetupNodeDistanceMatrix();
	_SetupGraphletMatrix();

	printf("DataHolder: Max variable  node ID: %u\n", GetMaxVariableNodeID());
	printf("DataHolder: Max reference node ID: %u\n", GetMaxReferenceNodeID());

	printf("DataHolder: Max variable  node degree: %u\n", GetMaxVariableNodeDegree());
	printf("DataHolder: Max reference node degree: %u\n", GetMaxReferenceNodeDegree());

	printf("DataHolder: variable edge amount: %u\n", GetVariableEdgeAmount());
	printf("DataHolder: reference edge amount: %u\n", GetReferenceEdgeAmount());

	_GenerateDefaultMappings();

	// Must be after BLAST and graphlet data are available
	_CalcCombinedScores();

	_wasInit = true;
	printf("DataHolder: Done!\n");
	return true;
}

void DataHolder::_SetupGraphletMatrix()
{
	// Graphlet sigs matrix dumpfile checksum/filename generation
	std::string grletFile;
	{
		uint adler = adler32(0, NULL, 0);
		{
			const PNodeArray& gx = _ph->GetGroup(_settings.varGroup);
			for(size_t i = 0; i < gx.size(); ++i)
				adler = adler32(adler, gx[i]->name().c_str(), gx[i]->name().length() + 1); // include '\0'
		}
		{
			const PNodeArray& gy = _ph->GetGroup(!_settings.varGroup);
			for(size_t i = 0; i < gy.size(); ++i)
				adler = adler32(adler, gy[i]->name().c_str(), gy[i]->name().length() + 1); // include '\0'
		}
		char buf[128];
		sprintf(&buf[0], "grsigs-%u.bin", adler);
		grletFile = &buf[0];
	}

	if(_ph->UsingGraphletData())
	{
		printf("DataHolder: Setting up graphlet score lookup matrix (file: %s)...\n", grletFile.c_str());
		Matrix<score> grlet;
		if(!grlet.readFromFile(grletFile.c_str()))
		{
			_PrecalcGraphletScores(grlet);
			if(!_settings.noWorkfiles && grlet.writeToFile(grletFile.c_str()))
				printf("DataHolder: Dumped graphlet score lookup matrix to file %s\n", grletFile.c_str());
		}
		_lookupGraphletScore.load(grlet);
	}
	else
	{
		// Fallback
		const PNodeArray& gxv = _ph->GetGroup(_settings.varGroup);
		const PNodeArray& gyv = _ph->GetGroup(!_settings.varGroup);
		uint xmax = gxv.size() + 1;
		uint ymax = gyv.size() + 1;
		_lookupGraphletScore.resize(xmax, ymax);
		printf("DataHolder: No graphlet data present, using node distance values...\n");
		for(size_t y = 0; y < _lookupGraphletScore.height(); ++y)
			for(size_t x = 0; x < _lookupGraphletScore.width(); ++x)
				_lookupGraphletScore(x, y) = _lookupNodeDistanceScore(x, y);
	}
}

/*template <typename DST, typename SRC> static void _transferMatrix_MT(MTRand&, void *p1, void *p2, void*)
{
	DST& to = *((*DST)p1);
	SRC& from = *((*SRC)p2);
	to.load(from);
}*/

void DataHolder::_TransferConnectionMatrix(uint group)
{
	// Symmetric, no x/y swapping required
	printf("DataHolder: Transferring connection lookup matrix, group %u ...\n", group);
	FastMatrix<uchar>& cr = _lookupConnRow[group];
	FastMatrix<uchar>& cri = _lookupConnRowInv[group];
	FastMatrixColumnWise<uchar>& cc = _lookupConnCol[group];
	FastMatrixColumnWise<uchar>& cci = _lookupConnColInv[group];
	cr.load(_ph->GetConnMatrix(group));
	cc.load(_ph->GetConnMatrix(group));
	cri.resize(cr.width(), cr.height());
	cci.resize(cc.width(), cc.height());

	printf("DataHolder: Generating inverted matrix...\n");

	// Inverse/Bit-flipped connection lookup matrix.
	// 1 becomes 0, 0 becomes 1. The matrix can not contain any other values.
	// This is used to avoid XOR operations in calcPairGEDIndexed().
	for(uint y = 0; y < cr.height(); ++y)
		for(uint x = 0; x < cr.width(); ++x)
			cri(x, y) = 1 ^ cr(x, y);

	for(uint x = 0; x < cr.width(); ++x)
		for(uint y = 0; y < cr.height(); ++y)
			cci(x, y) = 1 ^ cc(x, y);
}

struct _ParamCarrier
{
	_ParamCarrier(uint m, const std::vector<ProteinNode*>& gxv_, const std::vector<ProteinNode*>& gyv_, Matrix<score>& m_)
		: bar(m), gxv(gxv_), gyv(gyv_), m(m_) {}
	ProgressBar bar;
	Lockable lock;
	const std::vector<ProteinNode*>& gxv;
	const std::vector<ProteinNode*>& gyv;
	Matrix<score>& m;
};

static void _CalcGrletMatrixRow(MTRand& /*unused*/, void *p1, void *p2, void *)
{
	uint iy = (uint)(intptr_t(p1));
	_ParamCarrier& prm = *((_ParamCarrier*)p2);
	Matrix<score>& m = prm.m;

	size_t xsize = prm.gxv.size();
	const ProteinNode *py = prm.gyv[iy];
	const uint y = py->id;

	for(size_t ix = 0; ix < xsize; ++ix)
	{
		const ProteinNode *px = prm.gxv[ix];
		const uint x = px->id;
		m(x, y) = calcGraphletDistance(px, py);
	}

	{
		MTGuard(prm.lock);
		prm.bar.Step();
	}
}

void DataHolder::_PrecalcGraphletScores(Matrix<score> &m)
{
	// Symmetric, no x/y swapping required (but doing it anyway, to be absolutely safe)

	const PNodeArray& gxv = _ph->GetGroup(_settings.varGroup);
	const PNodeArray& gyv = _ph->GetGroup(!_settings.varGroup);

	uint xmax = gxv.size() + 1;
	uint ymax = gyv.size() + 1;
	m.resize(xmax, ymax);

	printf("DataHolder: Precalculating graphlet scores...\n");

	_ParamCarrier prm(ymax, gxv, gyv, m);

	// null entries
	m(0, 0) = 0; // NULL vs NULL, can't be better

	// -- Reference group
	for(size_t iy = 0; iy < gyv.size(); ++iy)
	{
		const ProteinNode *py = gyv[iy];
		ASSERT(py->group != _settings.varGroup);
		uint y = py->id;
		ASSERT(y);
		m(0, y) = _settings.pairNullValue;
	}

	// -- Variable group
	for(size_t ix = 0; ix < gxv.size(); ++ix)
	{
		const ProteinNode *px = gxv[ix];
		ASSERT(px->group == _settings.varGroup);
		uint x = px->id;
		ASSERT(x);
		m(x, 0) = _settings.pairNullValue;
	}

	prm.bar.Step();

	if(_thpool)
	{
		_thpool->lock();
		for(size_t iy = 0; iy < gyv.size(); ++iy)
			_thpool->addJobUnsafe(_CalcGrletMatrixRow, (void*)(intptr_t(iy)), &prm); // HACK: abuse void* as int
		_thpool->unlock();
		_thpool->waitUntilEmpty();
	}
	else
	{
		MTRand *rngDummy = NULL; // dummy
		for(size_t iy = 0; iy < gyv.size(); ++iy)
			_CalcGrletMatrixRow(*rngDummy, (void*)(intptr_t(iy)), &prm, NULL);
	}
}

void DataHolder::_TransferBLASTScores()
{
	// BLAST matrix is NOT symmetric - bring it into a form that
	// the X axis is always the variable group, and Y the static reference group

	const PNodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const PNodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	uint xmax = gxv.size() + 1;
	uint ymax = gyv.size() + 1;

	_lookupBLASTScore.resize(xmax, ymax);

	if(!_ph->UsingBLASTData())
	{
		printf("DataHolder: No BLAST data present, using dummy entries...\n");
//LABEL_DUMMY:

		for(size_t y = 0; y < _lookupBLASTScore.height(); ++y)
			for(size_t x = 0; x < _lookupBLASTScore.width(); ++x)
				_lookupBLASTScore(x, y) = -1;
		return;
	}

	if (_settings.bioScoreType == "NULL"){
		printf("DataHolder: bioScoreType is not specified! Using dummy entries...\n");
		for(size_t y = 0; y < _lookupBLASTScore.height(); ++y)
			for(size_t x = 0; x < _lookupBLASTScore.width(); ++x)
				_lookupBLASTScore(x, y) = -1;
		//goto LABEL_DUMMY;
	} 



	printf("DataHolder: Transferring BLAST lookup matrix...\n");
	ProgressBar bar((uint)gyv.size());

	// null entries
	_lookupBLASTScore(0, 0) = 0; // NULL against NULL = perfect.

	double totalscore = 0;
	score highest = -1, lowest = -1;
	uint invalid = 0, valid = 0;

	for(size_t iy = 0; iy < gyv.size(); ++iy)
	{
		const ProteinNode *py = gyv[iy];
		const uint y = py->id;
		for(size_t ix = 0; ix < gxv.size(); ++ix)
		{
			const ProteinNode *px = gxv[ix];
			const uint x = px->id;
			// copying these might seem useless, but this other BLAST score
			// matrix might use a different addressing, which gets in the way when
			// using an int-vector to represent the nodes.
			// (Need *one* ID that fits them all).
			// Also, this new matrix is a FastMatrix which doubles the lookup speed.
			score sc = _ph->GetBlastScore(px, py);
			
			if(sc < 0)
			{
				++invalid;
				_lookupBLASTScore(x, y) = 1;
			}
			else
			{
				if (_settings.bioScoreType == "E"){
					_lookupBLASTScore(x, y) = (sc < _settings.thresholdBLAST ? 0: 1); /* introduced */
				} else if (_settings.bioScoreType == "S"){
					_lookupBLASTScore(x, y) = (sc > _settings.thresholdBLAST ? 0: 1); /* introduced */
				}

			}
		}
		bar.Step();
	}

	// -- Reference group
	for(size_t iy = 0; iy < gyv.size(); ++iy)
	{
		const ProteinNode *py = gyv[iy];
		ASSERT(py->group != _settings.varGroup);
		uint y = py->id;
		//_lookupBLASTScore(0, y) = _ph.GetBlastScore(NULL, py);
		//_lookupBLASTScore(0, y) = avg;
		_lookupBLASTScore(0, y) = 1; // Invalid/Unknown
	}

	// -- Variable group
	for(size_t ix = 0; ix < gxv.size(); ++ix)
	{
		const ProteinNode *px = gxv[ix];
		ASSERT(px->group == _settings.varGroup);
		uint x = px->id;
		//_lookupBLASTScore(x, 0) = _ph.GetBlastScore(px, NULL);
		//_lookupBLASTScore(x, 0) = avg;
		_lookupBLASTScore(x, 0) = 1; // Invalid/Unknown
	}

	printf("BLAST: Invalid Entries: %u  (%.3f%%)\n", invalid, (double(invalid) / (gxv.size() * gyv.size())) * 100);
}

// The neighbor vector is used in GED calculation, because the neighbor set is too slow
// when accessed linearly.
void DataHolder::_FillNeighborVector(uint group)
{
	printf("DataHolder: Setting up node neighbors vector, group %u ...\n", group);
	const PNodeArray& g = _ph->GetGroup(group);
	for(size_t i = 0; i < g.size(); ++i)
	{
		ProteinNode *node = g[i];
		node->allv.clear();
		node->allv.reserve(node->nb.size());

		node->outgoingv.clear();
		node->outgoingv.reserve(node->outgoing.size());

		node->incomingv.clear();
		node->incomingv.reserve(node->incoming.size());

		node->undirectedv.clear();
		node->undirectedv.reserve(node->undirectedv.size());

		for(NodeSet::iterator jt = node->nb.begin(); jt != node->nb.end(); ++jt)
		{
			node->allv.push_back((ProteinNode*)*jt);
			if(node->hasEdgeTo(*jt) && node->hasEdgeFrom(*jt))
				node->undirectedv.push_back((ProteinNode*)*jt);
		}

		// Make sure that no undirected node ends up in incomingv/outgoingv
		for(NodeSet::iterator jt = node->outgoing.begin(); jt != node->outgoing.end(); ++jt)
			if(!node->hasEdgeFrom(*jt))
				node->outgoingv.push_back((ProteinNode*)*jt);

		for(NodeSet::iterator jt = node->incoming.begin(); jt != node->incoming.end(); ++jt)
			if(!node->hasEdgeTo(*jt))
				node->incomingv.push_back((ProteinNode*)*jt);

		ASSERT(node->allv.size() == node->outgoingv.size() + node->incomingv.size() + node->undirectedv.size());
	}
}

void DataHolder::_GenerateDefaultMappings()
{
	printf("DataHolder: Generating default mappings...\n");

	uint refGroup = !_settings.varGroup;
	printf("DataHolder: Var group = %u, ref group = %u\n", _settings.varGroup, refGroup);

	_refMapping.clear();
	_defaultMapping.clear();

	const PNodeArray &ref = _ph->GetGroup(refGroup);
	_refMapping.clear();
	ASSERT(ref.size() == GetMaxReferenceNodeID());
	for(PNodeArray::const_iterator it = ref.begin(); it != ref.end(); ++it)
	{
		ASSERT((*it)->group == refGroup);
		_refMapping.push_back(*it);
		ASSERT((*it)->id == _refMapping.size()); // This property is important: Node ID - 1 == array position.
	}

	const PNodeArray &nodes = _ph->GetGroup(_settings.varGroup);
	ASSERT(nodes.size() == GetMaxVariableNodeID());
	for(PNodeArray::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		ASSERT((*it)->group == _settings.varGroup);
		_defaultMapping.push_back(*it);
		ASSERT((*it)->id == _defaultMapping.size()); // This property is important: Node ID - 1 == array position.
	}

	ASSERT(_refMapping.size() == GetMaxReferenceNodeID());
	ASSERT(_defaultMapping.size() == GetMaxVariableNodeID());

	size_t finalsize  = _refMapping.size() + _defaultMapping.size();
	_refMapping.resize(finalsize);
	_defaultMapping.resize(finalsize);
}

/*
void DataHolder::_CalcWorstGED()
{
	ASSERT(_refMapping.size() == _defaultMapping.size());
	NodeMapping m(_refMapping.size(), NULL);

	GEDInfo ged;
	ged.nodesChanged = 0;
	ged.edgesChanged = 0;

	for(size_t i = 0; i < m.size(); ++i)
		calcPairGED(*this, m, _defaultMapping, i, ged);
	for(size_t i = 0; i < m.size(); ++i)
		calcPairGED(*this, m, _refMapping, i, ged);

	// edges are counted twice, so this number *must* be even
	ASSERT(!(ged.edgesChanged & 1));
	ged.edgesChanged >>= 1;

	// TODO: copied this from agent. move to shared place in case of code change.
	_worstGEDRaw = ged.edgesChanged;

	printf("DataHolder: Calculated worst GED: %u\n", _worstGEDRaw);

	// Note: This is probably an overcomplicated way of calculating
	// the total amount of edges in both graphs.
	// This calculation is now done in _CalcMisc(), and a lot simpler.
}
*/

void DataHolder::_CalcMisc()
{
	const PNodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const PNodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	_nodeMaxID[0] = 0;
	_nodeMaxID[1] = 0;
	_nodeMaxDegree[0] = 0;
	_nodeMaxDegree[1] = 0;
	_totalEdgeAmount[0] = 0;
	_totalEdgeAmount[1] = 0;
	_firstNonPrematchedIndex = 0;

	bool undirected = true;
	bool prematched = true;

	// -- Reference group
	for(size_t i = 0; i < gyv.size(); ++i)
	{
		const ProteinNode *py = gyv[i];

		if(py->id > _nodeMaxID[!_settings.varGroup])
			_nodeMaxID[!_settings.varGroup] = py->id;

		if( ((uint)py->nb.size() + py->hasSelfLoop )> _nodeMaxDegree[!_settings.varGroup])
			_nodeMaxDegree[!_settings.varGroup] = (uint)py->nb.size() + py->hasSelfLoop;

		_totalEdgeAmount[!_settings.varGroup] += (uint)py->nb.size() + 2*py->hasSelfLoop;

		if(undirected)
		{
			// edges from py -> jn ...
			for(NodeSet::const_iterator jt = py->outgoing.begin(); jt != py->outgoing.end(); ++jt)
			{
				const Node *jn = *jt;
				// ... must also have an edge from jn -> py, otherwise there are also directed edges.
				ASSERT(jn->hasEdgeFrom(py));
				undirected = undirected && jn->hasEdgeTo(py);
			}
		}
	}

	// -- Variable group
	for(size_t i = 0; i < gxv.size(); ++i)
	{
		const ProteinNode *px = gxv[i];

		if(px->id > _nodeMaxID[_settings.varGroup])
			_nodeMaxID[_settings.varGroup] = px->id;

		if( ((uint)px->nb.size() + px->hasSelfLoop) > _nodeMaxDegree[_settings.varGroup])
			_nodeMaxDegree[_settings.varGroup] = (uint)px->nb.size() + px->hasSelfLoop;

		_totalEdgeAmount[_settings.varGroup] += (uint)px->nb.size() + 2*px->hasSelfLoop;

		if(undirected)
		{
			// edges from px -> jn ...
			for(NodeSet::const_iterator jt = px->outgoing.begin(); jt != px->outgoing.end(); ++jt)
			{
				const Node *jn = *jt;
				// ... must also have an edge from jn -> px, otherwise there are also directed edges.
				ASSERT(jn->hasEdgeFrom(px));
				undirected = undirected && jn->hasEdgeTo(px);
			}
		}

		// prematched nodes?
		if(prematched && !px->prematched)
		{
			prematched = false;
			_firstNonPrematchedIndex = i;
			printf("DataHolder: First not prematched index: %u\n", _firstNonPrematchedIndex);
		}
	}

	// Huh? Happens if a network is aligned against itself and prematching is turned on,
	// i.e. ALL nodes can be pre-matched
	if(prematched)
		_firstNonPrematchedIndex = std::min(gxv.size(), gyv.size());


	// Make sure both are called.
	bool prematchOk = _CheckPrematch(0);
	prematchOk = _CheckPrematch(1) && prematchOk;

	if(!prematchOk)
	{
		printf("DataHolder: WARNING: Node prematching is messed up, disabling that altogether.\n");
		_firstNonPrematchedIndex = 0;
		for(size_t i = 0; i < gxv.size(); ++i)
			gxv[i]->prematched = false;
		for(size_t i = 0; i < gyv.size(); ++i)
			gyv[i]->prematched = false;
	}

	if(undirected)
	{
		printf("DataHolder: Graphs have only undirected edges, can use faster GED calculation.\n");
		_hasDirectedEdges = false;
	}
	else
	{
		printf("DataHolder: Graphs have directed edges, using slower GED calculation.\n");
		_hasDirectedEdges = true;
	}

	_nodeMaxDegreeTotal = std::max(_nodeMaxDegree[0], _nodeMaxDegree[1]);

	ASSERT(_nodeMaxID[0]);
	ASSERT(_nodeMaxID[1]);

	// ensure even number (must be; each edge is seen twice)
	ASSERT((_totalEdgeAmount[0] & 1) == 0);
	ASSERT((_totalEdgeAmount[1] & 1) == 0);
	_totalEdgeAmount[0] /= 2;
	_totalEdgeAmount[1] /= 2;

	_worstGEDRaw = _totalEdgeAmount[0] + _totalEdgeAmount[1];

	printf("DataHolder: Total edge count: %u\n", _worstGEDRaw);

	score highestNode = -1, highestEdge = -1;
	for(uint i = GEDI_MIN_EDGE; i < GEDI_MAX_EDGE; ++i)
		if(_gedScores[i] > highestEdge)
			highestEdge = _gedScores[i];
	for(uint i = GEDI_MIN_NODE; i < GEDI_MAX_NODE; ++i)
		if(_gedScores[i] > highestNode)
			highestNode = _gedScores[i];

	_worstGEDScore = (_worstGEDRaw * highestEdge) + ((gxv.size() + gyv.size()) * highestNode);

	printf("DataHolder: Worst GED score: %f\n", _worstGEDScore);
}

bool DataHolder::_CheckPrematch(uint g)
{
	bool good = true;
	const PNodeArray& gv = _ph->GetGroup(g);
	const PNodeArray& go = _ph->GetGroup(!g);

	for(size_t i = 0; i < gv.size(); ++i)
	{
		ProteinNode *p = gv[i];

		if(i >= _firstNonPrematchedIndex && p->prematched)
		{
			// We may end up here if resuming a state where prematching was turned on later.
			// That way, nodes that would be prematched
			printf("DataHolder: WARNING: Prematched node id out-of-order, ignoring: %s in network %u\n",
				p->name().c_str(), g+1);
			p->prematched = false;
		}

		if(i < go.size())
		{
			ProteinNode *o = go[i];
			if(p->prematched != o->prematched)
			{
				printf("DataHolder: WARNING: Different prematch state at mapping index %u\n", (uint)i);
				p->prematched = false;
				o->prematched = false;
				good = false;
			}
		}
	}
	return good;
}

void DataHolder::_SetupNodeDistanceMatrix()
{
	const PNodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const PNodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	uint xmax = gxv.size() + 1;
	uint ymax = gyv.size() + 1;

	printf("DataHolder: Precalculating node distance scores...\n");
	ProgressBar bar(ymax);

	FastMatrix<score>& m = _lookupNodeDistanceScore;
	m.resize(xmax, ymax);
	
	// null entries
	m(0, 0) = 0; // NULL vs NULL, can't be better

	// -- Reference group
	for(size_t iy = 0; iy < gyv.size(); ++iy)
	{
		const ProteinNode *py = gyv[iy];
		ASSERT(py->group != _settings.varGroup);
		uint y = py->id;
		ASSERT(y);
		m(0, y) = _settings.pairNullValue;
	}

	// -- Variable group
	for(size_t ix = 0; ix < gxv.size(); ++ix)
	{
		const ProteinNode *px = gxv[ix];
		ASSERT(px->group == _settings.varGroup);
		uint x = px->id;
		ASSERT(x);
		m(x, 0) = _settings.pairNullValue;
	}

	bar.Step();

	for(size_t iy = 0; iy < gyv.size(); ++iy)
	{
		const ProteinNode *py = gyv[iy];
		const uint y = py->id;
		for(size_t ix = 0; ix < gxv.size(); ++ix)
		{
			const ProteinNode *px = gxv[ix];
			const uint x = px->id;
			m(x, y) = calcNodeDegreeDistance(px, py);
		}

		bar.Step();
	}
}

void DataHolder::_CalcCombinedScores()
{
	printf("DataHolder: Precalculating BLAST & graphlets combined score...\n");

	const PNodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const PNodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	uint xmax = gxv.size() + 1;
	uint ymax = gyv.size() + 1;

	_lookupCombinedScore.resize(xmax, ymax);

	const bool useSeqsim = _settings.pairWeightSeqSim > 0 && _lookupSeqSimScore.width(); // matrix size is zero if not used or invalid

	for(uint y = 0; y < ymax; ++y)
	{
		for(uint x = 0; x < xmax; ++x)
		{
			// First, try BLAST score.
			score s = _lookupBLASTScore((uint)x, (uint)y) * _settings.pairWeightBLAST;

			s += _lookupGraphletScore(x, y) * _settings.pairWeightGraphlets;

			s += _lookupNodeDistanceScore(x, y) * _settings.pairWeightNodeDist;

			if(useSeqsim)
				s += _lookupSeqSimScore(x, y) * _settings.pairWeightSeqSim;

			// GED can't be calculated here; it's added in Agent::getPairScore().

			// Also, we can't pre-scale the score here, this is done in Agent::getPairScore().

			_lookupCombinedScore(x, y) = s;
		}
	}

	// The BLAST score is not used anywhere nowhere else externally, this can be cleared to save space.
	_lookupBLASTScore.clear();
	printf("DataHolder: BLAST matrix cleared, no longer needed\n");
}

void DataHolder::_FillGEDScoreArray()
{
	_gedScores[GEDI_NONE] = 0; // <- The value at this index is never read.

	_gedScores[GEDI_ADD_UNDIRECTED] =
	_gedScores[GEDI_ADD_DIRECTED_1] =
	_gedScores[GEDI_ADD_DIRECTED_2] = 
	_gedScores[GEDI_ADD_SELF_LOOP]  = _settings.edgeAddedGEDScore;

	_gedScores[GEDI_REMOVE_UNDIRECTED] =
	_gedScores[GEDI_REMOVE_DIRECTED_1] =
	_gedScores[GEDI_REMOVE_DIRECTED_2] = 
	_gedScores[GEDI_REMOVE_SELF_LOOP]  =_settings.edgeRemovedGEDScore;

	_gedScores[GEDI_SUBST_UNDIRECTED] =
	_gedScores[GEDI_SUBST_DIRECTED_1] =
	_gedScores[GEDI_SUBST_DIRECTED_2] = 
	_gedScores[GEDI_SUBST_SELF_LOOP]  = _settings.edgeSubstitutedGEDScore;

	_gedScores[GEDI_FLIP_1] =
	_gedScores[GEDI_FLIP_2] = _settings.edgeFlippedGEDScore;

	_gedScores[GEDI_DIRECTED_TO_UNDIRECTED_1] =
	_gedScores[GEDI_DIRECTED_TO_UNDIRECTED_2] = _settings.edgeDirectedToUndirectedGEDScore;

	_gedScores[GEDI_UNDIRECTED_TO_DIRECTED_1] =
	_gedScores[GEDI_UNDIRECTED_TO_DIRECTED_2] = _settings.edgeUndirectedToDirectedGEDScore;

	_gedScores[GEDI_ADD_NODE] = _settings.nodeAddedGEDScore;

	_gedScores[GEDI_REMOVE_NODE] = _settings.nodeRemovedGEDScore;
}

bool DataHolder::_SetupSEQSIMMatrix()
{
	// Proceed only if this score is actually used.
	if(_settings.seqsimMatrixFile.empty() || !(_settings._pairCalcSeqSim || _settings._finalCalcSeqSim))
		return true;

	printf("DataHolder: Loading sequence similarity matrix (%s)\n", _settings.seqsimMatrixFile.c_str());

	std::string seqsimFile;
	{
		uint adler = adler32(0, NULL, 0);
		{
			const PNodeArray& gx = _ph->GetGroup(_settings.varGroup);
			for(size_t i = 0; i < gx.size(); ++i)
				adler = adler32(adler, gx[i]->name().c_str(), gx[i]->name().length() + 1); // include '\0'
		}
		{
			const PNodeArray& gy = _ph->GetGroup(!_settings.varGroup);
			for(size_t i = 0; i < gy.size(); ++i)
				adler = adler32(adler, gy[i]->name().c_str(), gy[i]->name().length() + 1); // include '\0'
		}
		char buf[128];
		sprintf(&buf[0], "seqsim-%u.bin", adler);
		seqsimFile = &buf[0];
	}
	
	Matrix<score> seqsim;
	if(!seqsim.readFromFile(seqsimFile.c_str()))
	{
		printf("DataHolder: Loading sequence similarity from text file (may take a while...)\n");
		if(!_ReadSEQSIMMatrixText(_settings.seqsimMatrixFile, _settings.seqsimMatrixLeftgroup, seqsim))
			return false;
		
		if(!_settings.noWorkfiles)
			seqsim.writeToFile(seqsimFile.c_str(), true);
	}

	_lookupSeqSimScore.load(seqsim);
	return true;
}

// "name1 name2 value"
// leftgroup is the group in which protein with name1 is.
bool DataHolder::_ReadSEQSIMMatrixText(const std::string& fn, uint leftgroup, Matrix<score>& M)
{
	ASSERT(leftgroup <= 1);
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadSEQSIMMatrix ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	uint xmax, ymax;
	{
		const PNodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
		const PNodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis
		xmax = gxv.size() + 1;
		ymax = gyv.size() + 1;
	}
	ProgressBar bar(xmax * ymax); // guess maximum (maybe less)
	M.resize(xmax, ymax);
	M.fill(_settings.pairNullValue);

	std::string line;
	std::string name1, name2;
	std::string cruft;
	uint linec = 0;
	bool good = true;

	while(std::getline(f,line))
	{
		++linec;
		chompNewline(line);
		if(line.empty())
			continue;
		std::istringstream in(line);
		score value = _settings.pairNullValue;
		in >> name1 >> name2 >> value;
		if(in >> cruft)
			printf("ReadSEQSIMMatrix WARNING: Excess data on line %u (expected 3 entries per line)\n", linec);

		// Sanitize input values
		if(value < 0)
			value = 0;
		else if(value > 1)
			value = 1;

		// Convert similarity into distance
		value = score(1) - value;

		fixProteinName(name1);
		fixProteinName(name2);

		ProteinNode *n1 = _ph->Get(leftgroup, name1, false);
		if(!n1)
		{
			printf("ReadSEQSIMMatrix ERROR: Unknown first protein on line %u: '%s'\n", linec, name1.c_str());
			good = false;
		}

		ProteinNode *n2 = _ph->Get(!leftgroup, name2, false);
		if(!n2)
		{
			printf("ReadSEQSIMMatrix ERROR: Unknown second protein on line %u: '%s'\n", linec, name2.c_str());
			good = false;
		}

		// Make sure the matrix is in the right order (varGroup always on X-axis)
		if(leftgroup == _settings.varGroup)
			M(n1->id, n2->id) = value;
		else
			M(n2->id, n1->id) = value;

		bar.Step();
	}

	f.close();

	if(good)
	{
		printf("ReadSEQSIMMatrix: ... Done!\n");
		return true;
	}

	printf("ReadSEQSIMMatrix: There were errors.\n");
	return false;
}

