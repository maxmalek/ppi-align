// gedevodll.cpp : Defines the exported functions for the DLL application.
//
#ifdef WIN32 
	#include "stdafx.h"
#endif

#include "gedevoplugin_GEDEVOJNI.h"


#include <jni.h>

#include <iostream>
#include <fstream>

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>




 #include "common.h"
 #include "Evolution.h"
 #include "ProgramState.h"
 #include "ResultPrinter.h"

 #include "Threading.h"
 #include "CmdlineParser.h"
 #include "CmdHelp.h"

 #include <signal.h>
 #include <fstream>


void onSignal(int s)
{
	fprintf(stderr, "Caught signal %d\n", s);
	exit(s);
}

static void usage()
{
	puts("GEDEVO v0.1 -- Builtin help:");
///	CmdHelp::Print();
}

char **split_to_array(char * arguments, int *pargc){
	char **argv;
	char *p;
	const char *seps = "\t";
	*pargc=0;
	
	argv = (char **)malloc(sizeof(char *)*strlen(arguments));
	
	//printf("%s\n", input);
	p = strtok(arguments, seps);
	while(p != NULL){
		argv[(*pargc)] = p;
		(*pargc)++;
		//printf("\t%s\t\t%s\n", p, input);
		
		p = strtok(NULL, seps);
	}
	
	return argv;
}

 bool runGEDEVOCore(int argc, char **argv){

	 if(argc <= 1)
	 {
		 usage();
		 return 0;
	 }

	 signal(SIGFPE, onSignal);

	 const uint SHOW_BEST_AMOUNT = 5;

	 ThreadPool thpool;
	 ProgramState state(&thpool);

	 CmdlineParser cmdline(argc, argv);
	 if(!cmdline.Apply(state))
	 {
		 puts("Run the program with no parameters for a quick help.\n");
		 return 2;
	 }


	 Evo evo(state.dh, &thpool);
	 AgentMgr amgr(state.dh);
	 evo.Link(amgr);

	 bool good = state.Init(evo);
	 if(good)
	 {
		 printf("Doing some evolution...\n");

		 const AgentArray& arr = evo.GetAgents();
		 bool perfect = false;
		 bool stop = false;

		 while(!(perfect || stop || ProgramState::MustQuit()))
		 {
			 state.NextIteration();
			 evo.Update();

			 printf("Done round #%u, %u agents alive\n", state.GetIterationCount(), (uint)arr.size());
			 Agent *top = arr[0];
			 const uint limit = std::min<uint>(SHOW_BEST_AMOUNT, arr.size());
			 for(uint j = 0; j < limit; ++j)
			 {
				 Agent *a = arr[j];
				 score ec = a->getEdgeCorrectness();
				 perfect = perfect || (ec == 1);
				 printf("%2u  GED: %f, EC: %f, PS: %f, raw GED: [%u, %u] (life: %u, DT: %u)\n",
					 j, a->getGEDScore(), a->getEdgeCorrectness(), a->getPairSumScore(),
					 a->getGEDRawFull(), a->getGEDRawPartial(), a->getLife(), a->getDistanceTo(top));
			 }

			 stop = state.IsDone();
		 }

		 if(perfect)
		 {
			 printf("Done! Found perfect solution after %u iterations.\n", state.GetIterationCount());
		 }
		 else if(stop)
		 {
			 printf("Done, abort criterion reached after %u iterations.\n", state.GetIterationCount());
		 }

		 state.Shutdown();
	 }
	 else
	 {
		 printf("Something isn't right, exiting. Re-check your settings.\n");
	 }

	 evo.Unlink();

 }

void copyfiles(){

	 std::ifstream  src("c:\\testGedevo\\HSV-1_vs_EBV1.txt");
     std::ofstream  dst("c:\\testGedevo\\HSV-1_vs_EBV.txt");

     dst << src.rdbuf();
}

JNIEXPORT void JNICALL Java_gedevoplugin_GEDEVOJNI_entryPoint(JNIEnv *env, jobject obj, jstring javaString) {
	bool good = true;
	
	const char *nativeString = env->GetStringUTFChars(javaString, 0);

	int argc;
	char **argv, *arguments;
	
	arguments = (char *)malloc(sizeof(char)*strlen(nativeString));
	strcpy(arguments, nativeString);
	
	argv = split_to_array(arguments, &argc);
	printf("INITIAL line: %s\n\n", nativeString);
	printf("argc=%d\narguments:\n", argc);
	
	for(int i=0; i < argc; i++){
		printf("%d\t%s\n", i, argv[i]);
	}

	// UNCOMMENT HERE
	////runGEDEVOCore(argc, argv);

	// COMMENT HERE
	copyfiles();

	
	env->ReleaseStringUTFChars(javaString, nativeString);
	free(argv);
	free(arguments);
	return;// good ? 0 : 1;
}
 
