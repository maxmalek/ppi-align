#!/usr/local/bin/python3

# Quick hack to analyze a network given an alignment result
# and extract common connected subgraphs

import sys


def fixProteinName(s):
    return s.split("|")[0]
    

class Alignment:
    def __init__(self, file):
        self.mapping = {} # "fromName" => "toName"
        self.nmap = {} # fromNode => toNode
        self.net1 = None
        self.net2 = None
        with open(file, "r") as fh:
            for line in fh:
                line = line.strip()
                if not len(line):
                    continue
                if line.startswith("#"):
                    continue
                sp = line.split()
                if sp[0] == "-" or sp[1] == "-":
                    continue
                self.mapping[sp[0]] = sp[1]
        if not len(self.mapping):
            raise Exception("Error: Empty alignment!")
                
    def setNetworks(self, net1, net2):
        if not len(net1.nodes):
            raise Exception("Error: Network 1 is empty")
        if not len(net2.nodes):
            raise Exception("Error: Network 2 is empty")
        self.net1 = net1
        self.net2 = net2
        for a, b in self.mapping.items():
            na = self.net1.nodes[a] # will fail if not found
            nb = self.net2.nodes[b]
            assert(not na.partner)
            assert(not nb.partner)
            na.partner = nb
            nb.partner = na
            self.nmap[na] = nb
            
    def findCCs(self):
        cclist = []
        cc = None
        unseen = list(self.net1.nodes.values())
        seen = set()
        q = [] # for BFS
        
        while len(unseen):
            # if we are here, stack is empty. start new cc.
            assert(not len(q))
            begin = unseen.pop()
            if begin in seen:
                continue
            seen.add(begin)
            q.append(begin)

            while(len(q)):
             
                a = q.pop(0)
                b = a.partner
                assert(b == self.nmap.get(a))
                if not b:
                    continue
                
                if not cc:
                    cc = [(a, b)]
                
                for anext in a.outgoing:
                    if anext in seen: continue
                    bnext = anext.partner
                    assert(bnext == self.nmap.get(anext))
                    if not bnext: continue
                    if bnext in b.outgoing:
                        q.append(anext)
                        seen.add(anext)
                        cc.append((anext, bnext))
                        
                for anext in a.incoming:
                    if anext in seen: continue
                    bnext = anext.partner
                    assert(bnext == self.nmap.get(anext))
                    if not bnext: continue
                    if bnext in b.incoming:
                        q.append(anext)
                        seen.add(anext)
                        cc.append((anext, bnext))
                        
            if cc and len(cc):
                #print("Finished CC: %d nodes" % len(cc)) 
                cclist.append(cc)
                cc = None
                
        return cclist
        
        
        

class Node:
    def __init__(self, name):
        self.name = name
        self.outgoing = set()
        self.incoming = set()
        self.partner = None
        
    def hasLinkTo(self, node):
        return (node in self.outgoing) or (node in self.incoming)
        
    def __repr__(self):
        return "Node<" + self.name + ">"
        
class Network:
    def __init__(self):
        self.nodes = {} # "name" -> node
        
    def getNode(self, name, create = False):
        n = self.nodes.get(name)
        if create and not n:
            n = Node(name)
            self.nodes[name] = n
        return n
        
    def readNTW(self, file, undirected):
        with open(file, "r") as fh:
            for line in fh:
                line = line.strip()
                if not len(line):
                    continue
                if line.startswith("#"):
                    continue
                sp = line.split()
                # sp[0] contains the network name this edge belongs to, which is ignored for now

                p1 = fixProteinName(sp[1])
                p2 = fixProteinName(sp[2]) 
                na = self.getNode(p1, True)
                nb = self.getNode(p2, True)
                if na == nb: # skip self-loops
                    continue
                na.outgoing.add(nb)
                nb.incoming.add(na)
                if undirected:
                    na.incoming.add(nb)
                    nb.outgoing.add(na)
                    
        #print("readNTW: %d nodes" % len(self.nodes))
        return self
                    
                    
def analyzeResult(alnfile, net1, net2):

    print("Alignment: " + alnfile)
    aln = Alignment(alnfile)

    aln.setNetworks(net1, net2)
    res = aln.findCCs()

    ccmap = {}

    for cc in res:
        s = ccmap.get(len(cc))
        if not s:
            s = []
            ccmap[len(cc)] = s
        s.append(cc)
    
    #total = 0
    #for sz, s in sorted(ccmap.items()):
    #    print("Size %5d: %5d CCs" % (sz, len(s)))
    #    total += (sz * len(s))
    #print("Total: %d" % total)
    
    for sz, s in sorted(ccmap.items())[-3:]:
        print("Size %5d: %5d CCs" % (sz, len(s)))

    results = []
    for sz, s in sorted(ccmap.items(), reverse = True):
        for cc in s:
            results.append(cc)
        
    return results


# .sif file for analysis in cytoscape
def outputCommonGraphSIF(filename, cc, undirected):
    edges = set()
    et = " UndirectedEdge " if undirected else " DirectedEdge "
    with open(filename, "w") as fh:
        for a, b in cc:
            
            if not (a.partner == b):
                print(a)
                print(b)
                print(a.partner)
                assert(0)
                
            nn = a.name + "|" + b.name
            for an in a.outgoing:
                if an.partner in b.outgoing:
                    nn2 = an.name + "|" + an.partner.name
                    if not ((nn, nn2) in edges):
                        fh.write(nn + et + nn2 + "\n")
                        edges.add((nn, nn2))
                        if undirected:
                            edges.add((nn2, nn))





# === main code start ====

args = sys.argv[1:]

try:
    while args[0].startswith("-"):
        if args[0] == "-u":
            undirected = True
            args.pop(0)
        else:
            raise Exception("Error: Unknown commandline option: " + args[0])
        
    ntwfile1 = args[0]
    ntwfile2 = args[1]
    files = args[2:]
    fail = not len(files)
except:
    fail = True
    
if fail:
    print("Usage: " + sys.argv[0] + " [-u] ntwfile1 ntwfile2 alnfiles...")
    sys.exit(2)

results = {}
for f in files:
    # last part of path without extension
    basename = f.rsplit(".", 1)[0]
        
    # inefficient to load it again and again, but necessary to prevent clobbering node.partner
    net1 = Network().readNTW(ntwfile1, undirected)
    net2 = Network().readNTW(ntwfile2, undirected)
    results[basename] = analyzeResult(f, net1, net2)

alltotalbest = 0
alltotalccs = 0
alltotaltop5 = 0
for basename, res in results.items():
    outputCommonGraphSIF(basename + "_lccs.sif", res[0], undirected)
    
    perc5 = int(0.05 * len(res))
    c = 0
    uni5 = set()
    for i in range(perc5):
        c += len(res[i])
        uni5 = uni5.union(res[i])
    outputCommonGraphSIF(basename + "_ccs5perc.sif", uni5, undirected)
    
    alltotalbest += len(res[0])
    alltotalccs += len(res)
    alltotaltop5 += c
    
    minnodes = min(len(net1.nodes), len(net2.nodes))
    
    print("%s: LCCS: %d nodes (%.3f%%); %d nodes in top 5%%, %d total CCS" % (basename, len(res[0]), len(res[0]) / minnodes, c, len(res)))

print("Average LCCS: %f" % (alltotalbest / len(results)))
print("Average 5%% largest CCS: %f" % (alltotaltop5 / len(results)))
print("Average total CCS amount: %f" % (alltotalccs / len(results)))

