[OUTDATED -- Go to https://bitbucket.org/maxmalek/cytogedevo instead!]

This is the source code for the NABEECO & GEDEVO network alignment programs.
The code is released under the GPLv3 (see LICENSE.txt).
If you wish to use the code or parts of it in a context not compatible with the GPL,
contact Maximilian Malek or Jan Martens.

More information can be found on these websites:
* http://gedevo.mpi-inf.mpg.de
* http://nabeeco.mpi-inf.mpg.de

Author contact:
* Jan Martens      >> [not available]
* Maximilian Malek >> malek [ät] tugraz [döt] at
* Rashid Ibragimov >> [not available]


If you use the code or program in an academic or scientific background, please cite the below publications:

Rashid Ibragimov, Maximilian Malek, Jiong Guo, Jan Baumbach: GEDEVO: An Evolutionary Graph Edit Distance Algorithm for Biological Network Alignment. GCB 2013:68-79

Rashid Ibragimov, Jan Martens, Jiong Guo, and Jan Baumbach. 2013. NABEECO: biological network alignment with bee colony optimization algorithm. In Proceedings of the 15th annual conference companion on Genetic and evolutionary computation (GECCO '13 Companion), Christian Blum (Ed.). ACM, New York, NY, USA, 43-44. DOI=10.1145/2464576.2464600 http://doi.acm.org/10.1145/2464576.2464600




=== BUILDING ===

Tested on Windows, Linux, FreeBSD.
Uses SDL 1.2 or SDL2 for multithreading. (http://libsdl.org)

For building with Visual Studio 2008 on windows, there is a project file included.

For all other platforms use CMake to generate project or makefiles:

mkdir build
cd build
cmake ..   -OR-   ccmake ..  -OR-   cmake-gui ..
make